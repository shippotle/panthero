# Panthero
![Panthero](http://www.mwctoys.com/images1/review_panthro_1.jpg "This is Panthero")

# Description
Shippo's Platform API framework written in Typescript and MochaJS

## Requirements

- [Nodejs v14.17.5 (or newer lts) and npm 6.14.14 (or newer)](https://nodejs.org)


## Getting started

Install dependencies:

    $ npm install

## Before running the tests:
##### Ensure you have the appropriate API tokens (These are Platform specific Tokens)
##### Default Environment is QA, to run the tests against prod, set the environment variable:
    * Mac and Linux Users:
        * `export ENV=prod`
    * Windows Users(Powershell):
        * `$ENV:ENV="prod"`
  
##### Set API token:
    * Mac and Linux Users:
        * `export QA_TEST_TOKEN=<token>` or `export QA_LIVE_TOKEN=<token>`
        * `export PROD_TEST_TOKEN=<token>` or `export PROD_LIVE_TOKEN=<token>`
    * Windows Users(Powershell):
        * $ENV:QA_TEST_TOKEN=<token>` or `export $ENV:QA_LIVE_TOKEN=<token>`
        * `$ENV:PROD_TEST_TOKEN=<token>` or `export $ENV:PROD_LIVE_TOKEN=<token>`

#### Running tests:

    $ npm run test

#### Running a specific test

    $ npm run test <test file>

##### skipping a test suite

    describe.skip('My Wonderful test Suite')

##### skipping a test case

    it.skip('My Wonderful assertion')

##### Running TSLint:

    $ npm run tslint

##### Running Prettier:

    $ npm run prettier-format

##### Troubleshooting Tests - Service Logger:

    Developers can set the environment variable  "logger" to log service Requests and Responses out to the console terminal and inspect API calls.

Examples:

    logger="*request" - Logs Request.
    logger="*response" - Logs Response.
    logger="*info" - Logs both Request and response.
    logger="*request, *response" - Logs both Request and response.
    logger="*error" - Logs error Request and / or Response..

Mac/linux users:

    export logger="*request, *response"

Windows users(Powershell):

    $ENV:LOGGER="*request, *response"