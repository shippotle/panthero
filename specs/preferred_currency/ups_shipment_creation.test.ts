import { expect, IConfig, listOfCarriers } from '@utils/common.util';
import { AxiosResponse } from 'axios';
import { ShipmentData } from '@data/shipment.data';
import { MerchantService } from '@services/merchant-services';
import { carrierRegisteredWithUPS } from '@data/carrier-registered-merchants';
import { troubleShootPCCErrors } from '@utils/helpers';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Preferred Currency - ${listOfCarriers.ups.carrier.toUpperCase()} Positive Flows`, () => {
  const carrier = listOfCarriers.ups.carrier;
  let response: AxiosResponse;
  let s: ShipmentData;

  after('Reset all merchant to no currency', async () => {
    const service = new MerchantService();
    for (const merchant of carrierRegisteredWithUPS) {
      await new Promise((resolve) => setTimeout(resolve, 700));
      console.log(
        `Resetting use_preferred_currency to false for merchant: ${merchant}`
      );
      await service.putAlterMerchant(merchant, {
        use_preferred_currency: false
      });
    }
  });

  describe("As a Shippo Partner I should be able to ship domestically and see rates returned in USD (sender's default)", () => {
    let service: MerchantService;
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'US',
        stateTo: 'US',
        carrier,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      s = new ShipmentData(config);
      service = new MerchantService(config);
      response = await s.createAnInternationalShipment(
        config,
        carrierRegisteredWithUPS[0]
      );
      const responsebeforeTestRun = await service.getCarrierRegistration(
        s.merchantId
      );
      console.log(
        `Default Currency for MerchantId: ${s.merchantId} before Test Run: ${
          responsebeforeTestRun.data.preferred_currency
            ? responsebeforeTestRun.data
            : 'Sender Default'
        }`
      );
    });

    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('currency is the set to USD for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'USD');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(responseAfterTestRun.data);
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('USD');
      }
    });
    it('currency_local field is the set to USD for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'USD', 'currency_local');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(
          'Carrier Registration Staus after test run',
          responseAfterTestRun.data
        );
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('USD');
      }
    });
  });
  describe("As a Shippo Partner I should be able to ship to UK from US and see rates returned in USD (sender's default)", () => {
    let service: MerchantService;
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'US',
        stateTo: 'GB-UK',
        carrier,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      s = new ShipmentData(config);
      service = new MerchantService(config);
      response = await s.createAnInternationalShipment(
        config,
        carrierRegisteredWithUPS[1]
      );
      const responsebeforeTestRun = await service.getCarrierRegistration(
        s.merchantId
      );
      console.log(
        `Default Currency for MerchantId: ${s.merchantId} before Test Run: ${
          responsebeforeTestRun.data.preferred_currency
            ? responsebeforeTestRun.data.preferred_currency
            : 'Sender Default'
        }`
      );
    });

    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('currency is the set to USD for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'USD');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(responseAfterTestRun.data);
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('USD');
      }
    });
    it('currency_local field is the set to GBP for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'GBP', 'currency_local');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(
          'Carrier Registration Staus after test run',
          responseAfterTestRun.data
        );
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('GBP');
      }
    });
  });
  describe('As a Shippo Partner I should be able to ship to UK from US and see rates returned in preferred currency GBP', () => {
    let service: MerchantService;
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'US',
        stateTo: 'GB-UK',
        carrier,
        preferred_currency: 'GBP',
        use_preferred_currency: true,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      s = new ShipmentData(config);
      service = new MerchantService(config);
      response = await s.createAnInternationalShipment(
        config,
        carrierRegisteredWithUPS[2]
      );
      const responsebeforeTestRun = await service.getCarrierRegistration(
        s.merchantId
      );
      console.log(
        `Default Currency for MerchantId: ${s.merchantId} before Test Run: ${
          responsebeforeTestRun.data.preferred_currency
            ? responsebeforeTestRun.data.preferred_currency
            : 'Sender Default'
        }`
      );
    });

    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('currency is the set to GBP for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'GBP');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(responseAfterTestRun.data);
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('GBP');
      }
    });
    it('currency_local field is the set to GBP for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'GBP', 'currency_local');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(
          'Carrier Registration Staus after test run',
          responseAfterTestRun.data
        );
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('GBP');
      }
    });
  });
  describe("As a Shippo Partner I should be able to ship to Germany from US and see rates returned in USD (sender's default)", () => {
    let service: MerchantService;
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'US',
        stateTo: 'GER',
        carrier,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      s = new ShipmentData(config);
      service = new MerchantService(config);
      response = await s.createAnInternationalShipment(
        config,
        carrierRegisteredWithUPS[3]
      );
      const responsebeforeTestRun = await service.getCarrierRegistration(
        s.merchantId
      );
      console.log(
        `Default Currency for MerchantId: ${s.merchantId} before Test Run: ${
          responsebeforeTestRun.data.preferred_currency
            ? responsebeforeTestRun.data.preferred_currency
            : 'Sender Default'
        }`
      );
    });

    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('currency is the set to USD for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'USD');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('USD');
      }
    });
    it('currency_local field is the set to EUR for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'EUR', 'currency_local');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(
          'Carrier Registration Staus after test run',
          responseAfterTestRun.data
        );
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('EUR');
      }
    });
  });
  describe('As a Shippo Partner I should be able to ship to Germany from US and see rates returned in preferred currency EURO', () => {
    let service: MerchantService;
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'US',
        stateTo: 'GER',
        carrier,
        preferred_currency: 'EUR',
        use_preferred_currency: true,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      s = new ShipmentData(config);
      service = new MerchantService(config);
      response = await s.createAnInternationalShipment(
        config,
        carrierRegisteredWithUPS[4]
      );
      const responsebeforeTestRun = await service.getCarrierRegistration(
        s.merchantId
      );
      console.log(
        `Default Currency for MerchantId: ${s.merchantId} before Test Run: ${
          responsebeforeTestRun.data.preferred_currency
            ? responsebeforeTestRun.data.preferred_currency
            : 'Sender Default'
        }`
      );
    });

    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('currency is the set to EUR for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'EUR');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(
          'Carrier Registration Staus after test run',
          responseAfterTestRun.data
        );
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('EUR');
      }
    });
    it('currency_local field is the set to EUR for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'EUR', 'currency_local');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(responseAfterTestRun.data);
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('EUR');
      }
    });
  });
  describe('As a Shippo Partner I should be able to ship to Germany from US and see rates returned in preferred currency GBP', () => {
    let service: MerchantService;
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'US',
        stateTo: 'GER',
        carrier,
        preferred_currency: 'GBP',
        use_preferred_currency: true,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      s = new ShipmentData(config);
      service = new MerchantService(config);
      response = await s.createAnInternationalShipment(
        config,
        carrierRegisteredWithUPS[5]
      );
      const responsebeforeTestRun = await service.getCarrierRegistration(
        s.merchantId
      );
      console.log(
        `Default Currency for MerchantId: ${s.merchantId} before Test Run: ${
          responsebeforeTestRun.data.preferred_currency
            ? responsebeforeTestRun.data.preferred_currency
            : 'Sender Default'
        }`
      );
    });

    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('currency is the set to GBP for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'GBP');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(
          'Carrier Registration Staus after test run',
          responseAfterTestRun.data
        );
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('GBP');
      }
    });
    it('currency_local field is the set to EUR for each rate in rates', async () => {
      const error = troubleShootPCCErrors(response, 'EUR', 'currency_local');
      if (error) {
        const responseAfterTestRun = await service.getCarrierRegistration(
          s.merchantId
        );
        console.log(responseAfterTestRun.data);
        console.log(`MerchantId: ${s.merchantId}`);
        console.log(
          `Preferred Currency: ${
            responseAfterTestRun.data.preferred_currency
              ? responseAfterTestRun.data.preferred_currency
              : 'Sender Default'
          }`
        );
        console.log(
          'Shipment Response:',
          JSON.stringify(response.data, null, 2)
        );
      }
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('EUR');
      }
    });
  });
});
