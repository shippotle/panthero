import { envs, expect, IConfig, listOfCarriers } from '@utils/common.util';
import { AxiosResponse } from 'axios';
import { ShipmentData } from '@data/shipment.data';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Preferred Currency - ${listOfCarriers.canada.carrier.toUpperCase()} Positive Flows`, () => {
  const carrier = listOfCarriers.canada.carrier;
  let response: AxiosResponse;
  describe("As a Shippo Partner I should be able to ship domestically and see rates returned in CAD (sender's default)", () => {
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'CAN',
        stateTo: 'CAN',
        carrier,
        live_token: envs?.live_token,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      const s = new ShipmentData(config);
      response = await s.createAShipment(config);
    });
    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it(`currency is the set to CAD for each rate in rates`, () => {
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('CAD');
      }
    });
    it(`currency_local field is the set to CAD for each rate in rates`, () => {
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('CAD');
      }
    });
  });
  describe("As a Shippo Partner I should be able to ship to US from CANADA and see rates returned in USD (sender's default)", () => {
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'CAN',
        stateTo: 'US',
        carrier,
        live_token: envs?.live_token,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      const s = new ShipmentData(config);
      response = await s.createAnInternationalShipment(config);
    });
    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('currency is the set to CAD for each rate in rates', () => {
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('CAD');
      }
    });
    it('currency_local field is the set to USD for each rate in rates', () => {
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('USD');
      }
    });
  });
  describe('As a Shippo Partner I should be able to ship to UK from CANADA and see rates returned in preferred currency GBP', () => {
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'CAN',
        stateTo: 'GB-UK',
        carrier,
        preferred_currency: 'GBP',
        use_preferred_currency: true,
        live_token: envs?.live_token,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      const s = new ShipmentData(config);
      response = await s.createAnInternationalShipment(config);
    });
    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('currency is the set to GBP for each rate in rates', () => {
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('GBP');
      }
    });
    it('currency_local field is the set to GBP for each rate in rates', () => {
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('GBP');
      }
    });
  });
  describe("As a Shippo Partner I should be able to ship to Germany from CANADA and see rates returned in CAD (sender's default)", () => {
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'CAN',
        stateTo: 'GER',
        carrier,
        live_token: envs?.live_token,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      const s = new ShipmentData(config);
      response = await s.createAnInternationalShipment(config);
    });
    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('currency is the set to CAD for each rate in rates', () => {
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('CAD');
      }
    });
    it('currency_local field is the set to EUR for each rate in rates', () => {
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('EUR');
      }
    });
  });
  describe('As a Shippo Partner I should be able to ship to Germany from CANADA and see rates returned in preferred currency EURO', () => {
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'CAN',
        stateTo: 'GER',
        carrier,
        preferred_currency: 'EUR',
        use_preferred_currency: true,
        live_token: envs?.live_token,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      const s = new ShipmentData(config);
      response = await s.createAnInternationalShipment(config);
    });
    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('currency is the set to EUR for each rate in rates', () => {
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('EUR');
      }
    });
    it('currency_local field is the set to EUR for each rate in rates', () => {
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('EUR');
      }
    });
  });
  describe('As a Shippo Partner I should be able to ship to Germany from CANADA and see rates returned in preferred currency GBP', () => {
    before('Create a shipment', async () => {
      const config: IConfig = {
        stateFrom: 'CAN',
        stateTo: 'GER',
        carrier,
        preferred_currency: 'GBP',
        use_preferred_currency: true,
        live_token: envs?.live_token,
        parcels: [
          {
            length: '10.00',
            width: '10.00',
            height: '10.00',
            distance_unit: 'cm',
            weight: '1.670',
            mass_unit: 'lb'
          }
        ]
      };
      const s = new ShipmentData(config);
      response = await s.createAnInternationalShipment(config);
    });
    it('shipment has been created - 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('shipment has returned rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('currency is the set to GBP for each rate in rates', () => {
      for (const rate of response.data.rates) {
        expect(rate.currency).eq('GBP');
      }
    });
    it('currency_local field is the set to EUR for each rate in rates', () => {
      for (const rate of response.data.rates) {
        expect(rate.currency_local).eq('EUR');
      }
    });
  });
});
