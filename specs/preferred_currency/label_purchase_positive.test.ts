import { envs, expect, IConfig, listOfCarriers } from '@utils/common.util';
import { AxiosResponse } from 'axios';
import { ShipmentData } from '@data/shipment.data';
const currencies = require('@data/currencies-data.json');
import { pickMultipleRandomElementFromAList } from '@utils/helpers';
import { ShipmentService } from '@services/shipment-service';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Preferred Currency - Positive Flows`, () => {
  // tslint:disable-next-line:forin
  for (const key in listOfCarriers) {
    const country = listOfCarriers[key as keyof typeof listOfCarriers].country;
    const carrier = listOfCarriers[key as keyof typeof listOfCarriers].carrier;
    const currency_local =
      listOfCarriers[key as keyof typeof listOfCarriers].preferred_currency;
    if (carrier !== 'ups') {
      describe('Create Shipments', () => {
        let response: AxiosResponse;
        describe('CURRENCY SENDER DEFAULTS', () => {
          const currency =
            listOfCarriers[key as keyof typeof listOfCarriers]
              .preferred_currency;
          const preferred_currency =
            listOfCarriers[key as keyof typeof listOfCarriers]
              .preferred_currency;
          describe(`As a Shippo Partner I should be able to create a shipment for ${carrier.toUpperCase()} and see rates returned as senders default's currency: ${preferred_currency}`, () => {
            let s: ShipmentData;
            const config: IConfig = {
              stateFrom: country,
              stateTo: country,
              carrier,
              live_token: envs?.live_token,
              parcels: [
                {
                  length: '10.00',
                  width: '10.00',
                  height: '10.00',
                  distance_unit: 'cm',
                  weight: '1.670',
                  mass_unit: 'lb'
                }
              ]
            };
            before('Create a shipment', async () => {
              s = new ShipmentData(config);
              response = await s.createAShipment(config);
            });
            it('shipment has been created - 201 Created', () => {
              expect(response.status).eq(201);
            });
            it('shipment has returned rates', () => {
              expect(response.data.rates.length > 0).to.be.true;
            });
            it(`currency is the set to ${currency} for each rate in rates`, () => {
              for (const rate of response.data.rates) {
                expect(rate.currency).eq(currency);
              }
            });
            it(`currency_local field is the set to ${currency} for each rate in rates`, () => {
              for (const rate of response.data.rates) {
                expect(rate.currency_local).eq(currency);
              }
            });
          });
        });
      });
      for (const currency of pickMultipleRandomElementFromAList(
        currencies,
        20
      )) {
        describe('CURRENCY DIFFERENT THAN SENDER DEFAULTS', () => {
          let response: AxiosResponse;
          let s: ShipmentData;
          const preferred_currency = currency.code;
          const config: IConfig = {
            stateFrom: country,
            stateTo: country,
            preferred_currency,
            use_preferred_currency: true,
            live_token: envs?.live_token,
            carrier,
            parcels: [
              {
                length: '10.00',
                width: '10.00',
                height: '10.00',
                distance_unit: 'cm',
                weight: '1.670',
                mass_unit: 'lb'
              }
            ]
          };
          describe(`As a Shippo Partner I should be able to create a shipment for ${carrier.toUpperCase()} and see rates returned as per set currency: ${preferred_currency} (${
            currency.name
          })`, () => {
            before('Create a shipment', async () => {
              s = new ShipmentData(config);
              response = await s.createAShipment(config);
            });
            it('shipment has been created - 201 Created', () => {
              expect(response.status).eq(201);
            });
            it('shipment has returned rates', () => {
              expect(response.data.rates.length > 0).to.be.true;
            });
            it(`currency is the set to ${preferred_currency} for each rate in rates`, () => {
              for (const rate of response.data.rates) {
                expect(rate.currency).eq(preferred_currency);
              }
            });
            it(`currency_local field is the set to ${currency_local} for each rate in rates`, () => {
              for (const rate of response.data.rates) {
                expect(rate.currency_local).eq(currency_local);
              }
            });
          });
        });
        describe(`As a Shippo Partner after creating a shipment for ${carrier.toUpperCase()}, I should see the currency_local set to ${
          currency.code
        } (${currency.name}) for each rate`, () => {
          let responseGetRate: AxiosResponse;
          let s: ShipmentData;
          const config: IConfig = {
            stateFrom: country,
            stateTo: country,
            use_preferred_currency: true,
            live_token: envs?.live_token,
            carrier,
            parcels: [
              {
                length: '10.00',
                width: '10.00',
                height: '10.00',
                distance_unit: 'cm',
                weight: '1.670',
                mass_unit: 'lb'
              }
            ]
          };
          before(
            'Create a shipment and call GET /merchants/${merchantId}/shipments/${shipmentId}/rates/${currencyCode}',
            async () => {
              const ss = new ShipmentService(config);
              s = new ShipmentData(config);
              const response = await s.createAShipment(config);
              responseGetRate = await ss.getShipmentRates(
                s.merchantId,
                response.data.object_id,
                currency.code
              );
            }
          );
          it('rates have been returned - 200 OK', () => {
            expect(responseGetRate.status).eq(200);
          });
          it(`currency field is the set to ${currency_local} for each rate in rates`, () => {
            for (const rate of responseGetRate.data.results) {
              expect(rate.currency).eq(currency_local);
            }
          });
          it(`currency_local field is the set to ${currency.code} for each rate in rates`, () => {
            for (const rate of responseGetRate.data.results) {
              expect(rate.currency_local).eq(currency.code);
            }
          });
        });
      }
    }
  }
});
