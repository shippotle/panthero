import { expect } from '@utils/common.util';
import { httpRequest } from '@utils/helpers';
import { AxiosResponse } from 'axios';
import { ShipmentData } from '@data/shipment.data';
import { shipmentsResponseSchema } from '@schemas/shipments-schema';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Shipment API - Positive Flows`, () => {
  let response: any;
  describe('As a Shippo Partner (Squarespace) I should be able to create a shipment for my merchants', () => {
    before('Create Shipment', async () => {
      const s = new ShipmentData();
      response = await s.createAShipment();
    });
    it('should create a shipment - 201 Create', () => {
      expect(response.status).eq(201);
    });
    it(
      'should return the correct ' +
        'schema (ajv checks all types and structures)',
      () => {
        expect(response.data).to.be.jsonSchema(shipmentsResponseSchema);
      }
    );
    it('should return with some rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to create a shipment for my merchants without registering for a carrier (no rates returned)', () => {
    before('Create Shipment', async () => {
      const s = new ShipmentData();
      response = await s.createAShipmentWithoutACarrierRegistration(true);
    });
    it('should create a shipment - 201 Create', () => {
      expect(response.status).eq(201);
    });
    it('should return with some rates', () => {
      expect(response.data.rates.length === 0).to.be.true;
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to retrieve a shipments for my merchants', () => {
    let getResponse: AxiosResponse;
    let shipmentId: AxiosResponse;
    before('Create Shipment', async () => {
      const s = new ShipmentData();
      response = await s.createAShipment();
      shipmentId = response.data.object_id;
      getResponse = await httpRequest(
        `${s.shipment.shipmentsUrl(s.merchantId)}${response.data.object_id}`,
        'GET',
        s.shipment.headers(s.merchantId)
      );
    });
    it('should return a 200 OK', () => {
      expect(getResponse.status).eq(200);
    });
    it('should return with the same shipmentsId as per request', () => {
      expect(getResponse.data.object_id).eq(shipmentId);
    });
    it('should return with the  status = SUCCESS', () => {
      expect(getResponse.data.status).eq('SUCCESS');
    });
    it('should return with some rates', () => {
      expect(getResponse.data.rates.length > 0).to.be.true;
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to create a shipment for my merchants with an insurance addOn', () => {
    let insuranceAddOn: any = {};
    before('Create Shipment', async () => {
      insuranceAddOn = {
        extra: {
          insurance: {
            currency: 'USD',
            amount: '100'
          }
        }
      };
      const s = new ShipmentData();
      response = await s.createAShipmentWithAddOn(insuranceAddOn);
    });
    it('should create a shipment - 201 Create', () => {
      expect(response.status).eq(201);
    });
    it('should return with some rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('should return with the correct addOn Information rates', () => {
      expect(response.data.extra).to.deep.equalInAnyOrder(insuranceAddOn.extra);
    });
  });
  describe.skip('As a Shippo Partner (Squarespace) I should be able to create a shipment for my merchants with a saturday delivery addOn', () => {
    let saturday_del: any = {};
    before('Create Shipment', async () => {
      saturday_del = {
        extra: {
          saturday_delivery: true
        }
      };
      const s = new ShipmentData();
      response = await s.createAShipmentWithAddOn(saturday_del);
    });
    it('should create a shipment - 201 Create', () => {
      expect(response.status).eq(201);
    });
    it('should return with some rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
    it('should return with the correct addOn Information rates', () => {
      expect(response.data.extra).to.deep.equalInAnyOrder(saturday_del.extra);
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to create a shipment for my merchants even if the "name" property is missing from either the address_from or address_to', () => {
    before('Create Shipment', async () => {
      const s = new ShipmentData();
      response = await s.createAShipmentWithAMissingProperty('name');
    });
    it('should create a shipment - 201 Create', () => {
      expect(response.status).eq(201);
    });
    it('should return with an empty name in the address_from', () => {
      expect(response.data.address_from.name === '').to.be.true;
    });
    it('should return with an empty name in the address_to', () => {
      expect(response.data.address_to.name === '').to.be.true;
    });
    it('should return with some rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to create a shipment for my merchants even if the "phone" property is missing from either the address_from or address_to', () => {
    before('Create Shipment', async () => {
      const s = new ShipmentData();
      response = await s.createAShipmentWithAMissingProperty('phone');
    });
    it('should create a shipment - 201 Create', () => {
      expect(response.status).eq(201);
    });
    it('should return with an empty phone in the address_from', () => {
      expect(response.data.address_from.phone === '').to.be.true;
    });
    it('should return with an empty phone in the address_to', () => {
      expect(response.data.address_to.phone === '').to.be.true;
    });
    it('should return with some rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to create a shipment for my merchants even if the "email" property is missing from either the address_from or address_to', () => {
    before('Create Shipment', async () => {
      const s = new ShipmentData();
      response = await s.createAShipmentWithAMissingProperty('email');
    });
    it('should create a shipment - 201 Create', () => {
      expect(response.status).eq(201);
    });
    it('should return with an empty email in the address_from', () => {
      expect(response.data.address_from.email === '').to.be.true;
    });
    it('should return with an empty email in the address_to', () => {
      expect(response.data.address_to.email === '').to.be.true;
    });
    it('should return with some rates', () => {
      expect(response.data.rates.length > 0).to.be.true;
    });
  });
});
