import { expect, IConfig } from '@utils/common.util';
import { AxiosResponse } from 'axios';
import { ShipmentData } from '@data/shipment.data';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Shipment API - Negative Flows`, () => {
  const config: IConfig = {
    isPositiveFlow: false,
    carrier: 'ups'
  };
  describe('As a Shippo Partner (Squarespace) I should NOT be able to create a shipment for my merchants if the the address_from is missing or empty', () => {
    let response: AxiosResponse;
    before('Create a shipment', async () => {
      const s = new ShipmentData(config);
      response = await s.createAShipmentWithAMissingProperty('address_from');
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('response returns the correct error message (address_from)', () => {
      expect(response.data.address_from[0]).eq('This field is required.');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should NOT be able to create a shipment for my merchants if the the address_to is missing or empty', () => {
    let response: AxiosResponse;
    before('Create a shipment', async () => {
      const s = new ShipmentData(config);
      response = await s.createAShipmentWithAMissingProperty('address_to');
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('response returns the correct error message (address_to)', () => {
      expect(response.data.address_to[0]).eq('This field is required.');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should NOT be able to create a shipment for my merchants if the the parcels is missing or empty', () => {
    let response: AxiosResponse;
    before('Create a shipment', async () => {
      const s = new ShipmentData(config);
      response = await s.createAShipmentWithAMissingProperty('parcels');
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('response returns the correct error message (parcels)', () => {
      expect(response.data.parcels[0]).eq('This field is required.');
    });
  });
  // TODO
  describe.skip('As a Shippo Partner (Squarespace) I should NOT be able to create a shipment for my merchants if the city is missing or empty from the address_to or address_from', () => {
    let response: AxiosResponse;
    before('Create a shipment', async () => {
      const s = new ShipmentData(config);
      response = await s.createAShipmentWithAMissingProperty(['city', 'zip']);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
  });
});
