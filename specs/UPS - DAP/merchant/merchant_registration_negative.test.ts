import { expect } from '@utils/common.util';
import { httpRequest } from '@utils/helpers';
import { AxiosResponse } from 'axios';
import { merchantsErrorSchema } from '@schemas/merchants-schema';
import { MerchantService, IMerchant } from '@services/merchant-services';
import {
  distance_unit,
  mass_unit,
  ParcelServices
} from '@services/parcel-servcies';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Merchant API - Negative Flows`, () => {
  describe('As a Shippo Partner (Squarespace) I should be NOT able to register my merchants omitting the email information from the request body', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        first_name: 'John',
        last_name: 'Smith',
        merchant_name: 'My shop'
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
    it('response returns the correct error message (email)', () => {
      expect(response.data.email[0]).eq('This field is required.');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be NOT able to register my merchants omitting the first_name information from the request body', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        email: 'negativeFlows@goshippo.com',
        last_name: 'Smith',
        merchant_name: 'My shop'
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
    it('response returns the correct error message (first_name)', () => {
      expect(response.data.first_name[0]).eq('This field is required.');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be NOT able to register my merchants omitting the last_name information from the request body', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        email: 'negativeFlows@goshippo.com',
        first_name: 'John',
        merchant_name: 'My shop'
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
    it('response returns the correct error message (last_name)', () => {
      expect(response.data.last_name[0]).eq('This field is required.');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be NOT able to register my merchants omitting the merchant_name information from the request body', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        email: 'negativeFlows@goshippo.com',
        first_name: 'John',
        last_name: 'John'
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
    it('response returns the correct error message (merchant_name)', () => {
      expect(response.data.merchant_name[0]).eq('This field is required.');
    });
  });
  describe('Addresses - Negative flows', () => {
    describe('As a Shippo Partner I should NOT be able to set an address for a Merchants without country', () => {
      let responseAddress: AxiosResponse;
      let body: IMerchant;
      let merchantId: string = '';
      const bodyAddress = {
        company: 'Shippo',
        email: 'shippotle@goshippo.com',
        is_residential: true,
        metadata: 'Customer ID 123456',
        name: 'Shwan Ippotle',
        phone: '0015553419393',
        state: 'CA',
        street1: '215 Clayton St',
        street2: '',
        validate: true,
        zip: '94117-1913'
      };
      before('HTTP request to /merchants/', async () => {
        const merchant = new MerchantService();
        body = merchant.createNewMerchant();
        const responseMerchant = await merchant.postCreateNewMerchant(body);
        merchantId = responseMerchant.data.object_id;
      });
      before(
        'HTTP request to POST /merchants/${merchantId}/addresses/',
        async () => {
          const m = new MerchantService();
          responseAddress = await httpRequest(
            m.addressUrl(merchantId),
            'POST',
            m.headers(merchantId),
            bodyAddress
          );
        }
      );
      it('should return a 400 Bad Request', () => {
        expect(responseAddress.status).eq(400);
      });
      it('should return the correct schema (ajv checks all types and structures)', () => {
        expect(responseAddress.data).to.be.jsonSchema(merchantsErrorSchema);
      });
      it('response returns the correct error message (country)', () => {
        expect(responseAddress.data.country[0]).eq(
          'In order to create an Address, at least a country must be provided.'
        );
      });
    });
  });
  describe('Parcels - Negative flows', () => {
    describe('As a Shippo Partner I should NOT be able to create a custom parcel without mass_unit', () => {
      let responseParcel: AxiosResponse;
      let merchantId: string = '';
      const parcelBody = {
        distance_unit: distance_unit.in,
        extra: {
          COD: {
            amount: '5.50',
            currency: 'USD',
            payment_method: 'CASH'
          },
          insurance: {
            amount: '5.50',
            content: 'Laptop',
            currency: 'USD',
            provider: 'UPS'
          }
        },
        height: '1',
        length: '1',
        metadata: '',
        template: 'USPS_LargeFlatRateBox',
        test: true,
        weight: '1',
        width: '1'
      };
      before('HTTP request to /merchants/', async () => {
        const merchant = new MerchantService();
        const body = merchant.createNewMerchant();
        const responseMerchant = await merchant.postCreateNewMerchant(body);
        merchantId = responseMerchant.data.object_id;
      });
      before('HTTP request to /merchants/{{merchantId}}/parcels/', async () => {
        const parcel = new ParcelServices();
        responseParcel = await parcel.postCreateParcel(merchantId, parcelBody);
      });

      it('should return a 400 Bad Request', () => {
        expect(responseParcel.status).eq(400);
      });
      it('should return the correct schema (ajv checks all types and structures)', () => {
        expect(responseParcel.data).to.be.jsonSchema(merchantsErrorSchema);
      });
      it('response returns the correct error message (mass_unit)', () => {
        expect(responseParcel.data.mass_unit[0]).eq('This field is required.');
      });
    });
    describe('As a Shippo Partner I should NOT be able to create a custom parcel without weight', () => {
      let responseParcel: AxiosResponse;
      let merchantId: string = '';
      const parcelBody = {
        distance_unit: distance_unit.in,
        extra: {
          COD: {
            amount: '5.50',
            currency: 'USD',
            payment_method: 'CASH'
          },
          insurance: {
            amount: '5.50',
            content: 'Laptop',
            currency: 'USD',
            provider: 'UPS'
          }
        },
        height: '1',
        length: '1',
        mass_unit: mass_unit.lb,
        metadata: '',
        template: 'USPS_LargeFlatRateBox',
        test: true,
        width: '1'
      };
      before('HTTP request to /merchants/', async () => {
        const merchant = new MerchantService();
        const body = merchant.createNewMerchant();
        const responseMerchant = await merchant.postCreateNewMerchant(body);
        merchantId = responseMerchant.data.object_id;
      });
      before('HTTP request to /merchants/{{merchantId}}/parcels/', async () => {
        const parcel = new ParcelServices();
        responseParcel = await parcel.postCreateParcel(merchantId, parcelBody);
      });
      it('should return a 400 Bad Request', () => {
        expect(responseParcel.status).eq(400);
      });
      it('should return the correct schema (ajv checks all types and structures)', () => {
        expect(responseParcel.data).to.be.jsonSchema(merchantsErrorSchema);
      });
      it('response returns the correct error message (weight)', () => {
        expect(responseParcel.data.__all__[0]).eq(
          'Parcel weight and weight unit must be given.'
        );
      });
    });
  });
  describe('As a Shippo Partner I should be NOT able to register merchants omitting the preferred_currency property if use_preferred_currency is set to true', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        email: 'negativeFlows@goshippo.com',
        first_name: 'John',
        last_name: 'S',
        merchant_name: 'The Platform Test',
        use_preferred_currency: true
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('response returns the correct error message', () => {
      expect(response.data.preferred_currency[0]).eq(
        'Invalid ISO currency code'
      );
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
  });
  describe('As a Shippo Partner I should be NOT able to register merchants with a preferred_currency set to EEE(incorrect/invalid code)', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        email: 'negativeFlows@goshippo.com',
        first_name: 'John',
        last_name: 'S',
        merchant_name: 'The Platform Test',
        preferred_currency: 'EEE',
        use_preferred_currency: true
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('response returns the correct error message', () => {
      expect(response.data.preferred_currency[0]).eq(
        'Invalid ISO currency code'
      );
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
  });
  describe('As a Shippo Partner I should be NOT able to register merchants with a preferred_currency set to 123(incorrect/invalid code)', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        email: 'negativeFlows@goshippo.com',
        first_name: 'John',
        last_name: 'S',
        merchant_name: 'The Platform Test',
        preferred_currency: 123,
        use_preferred_currency: true
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('response returns the correct error message', () => {
      expect(response.data.preferred_currency[0]).eq(
        'This field must be of type string'
      );
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
  });
  describe('As a Shippo Partner I should be NOT able to register merchants with a preferred_currency set to *^%(incorrect/invalid code)', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        email: 'negativeFlows@goshippo.com',
        first_name: 'John',
        last_name: 'S',
        merchant_name: 'The Platform Test',
        preferred_currency: '*^%',
        use_preferred_currency: true
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('response returns the correct error message', () => {
      expect(response.data.preferred_currency[0]).eq(
        'Invalid ISO currency code'
      );
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
  });
  describe('As a Shippo Partner I should be NOT able to register merchants with a preferred_currency set to very long string (e.g. "A" x 100)', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        email: 'negativeFlows@goshippo.com',
        first_name: 'John',
        last_name: 'S',
        merchant_name: 'The Platform Test',
        preferred_currency:
          'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA',
        use_preferred_currency: true
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('response returns the correct error message', () => {
      expect(response.data.preferred_currency[0]).eq(
        'Invalid ISO currency code'
      );
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
  });
  describe('As a Shippo Partner I should be NOT able to register merchants with a preferred_currency set to null(incorrect)', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        email: 'negativeFlows@goshippo.com',
        first_name: 'John',
        last_name: 'S',
        merchant_name: 'The Platform Test',
        preferred_currency: null,
        use_preferred_currency: true
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('response returns the correct error message', () => {
      expect(response.data.preferred_currency[0]).eq(
        'Invalid ISO currency code'
      );
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
  });
  describe('As a Shippo Partner I should be NOT able to register merchants with a preferred_currency set to empty value(incorrect)', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        email: 'negativeFlows@goshippo.com',
        first_name: 'John',
        last_name: 'S',
        merchant_name: 'The Platform Test',
        preferred_currency: '',
        use_preferred_currency: true
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('response returns the correct error message', () => {
      expect(response.data.preferred_currency[0]).eq(
        'Invalid ISO currency code'
      );
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
  });
  describe('As a Shippo Partner I should NOT be able to register merchants with a use_preferred_currency set to a string(incorrect)', () => {
    let response: AxiosResponse;
    before('HTTP request to POST /merchants/', async () => {
      const body = {
        email: 'negativeFlows@goshippo.com',
        first_name: 'John',
        last_name: 'S',
        merchant_name: 'The Platform Test',
        preferred_currency: 'GBP',
        use_preferred_currency: 'bla'
      };
      const m = new MerchantService();
      response = await httpRequest(m.merchantUrl(), 'POST', m.headers(), body);
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('response returns the correct error message', () => {
      expect(response.data.use_preferred_currency[0]).eq(
        'This field must be of type boolean'
      );
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
  });
});
