import { expect, headers, listOfCarriers, IConfig } from '@utils/common.util';
import { httpRequest } from '@utils/helpers';
import { MerchantService, IMerchant } from '@services/merchant-services';
import { ParcelServices } from '@services/parcel-servcies';
import { AxiosResponse } from 'axios';
import {
  merchantsResponseSchema,
  AllmerchantsResponseSchema,
  merchantsAddress,
  AllmerchantsAddressResponseSchema,
  merchantsParcel,
  merchantRegistrationSchema
} from '@schemas/merchants-schema';
import { CarrierRegistrationData } from '@data/carrier-registration.data';

const currencies = require('@data/currencies-data.json');

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Merchant API - Positive Flows`, () => {
  let response: AxiosResponse;
  let body: IMerchant;
  let merchantId: string = '';

  describe('As a Shippo Partner (Squarespace) I should be able to register my merchants', () => {
    before('HTTP request to /merchants/', async () => {
      const merchant = new MerchantService();
      body = merchant.createNewMerchant();
      response = await merchant.postCreateNewMerchant(body);
      merchantId = response.data.object_id;
    });
    it('should return a 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsResponseSchema);
    });
    it('response returns the correct email', () => {
      expect(response.data.email).eq(body.email);
    });
    it('response returns the correct first_name', () => {
      expect(response.data.first_name).eq(body.first_name);
    });
    it('response returns the correct last_name', () => {
      expect(response.data.last_name).eq(body.last_name);
    });
    it('response returns the correct merchant_name', () => {
      expect(response.data.merchant_name).eq(body.merchant_name);
    });
  });
  describe('As a Shippo Partner I should be able to change my merchant email', () => {
    before('HTTP request to PUT /merchants/{{merchantId}}', async () => {
      const merchant = new MerchantService();
      body.email = 'changeEmail@goshippo.com';
      const url = merchant.merchantUrl(merchantId);
      const h = headers();
      response = await httpRequest(url, 'PUT', h, body);
    });
    it('should return a 200 ok', () => {
      expect(response.status).eq(200);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsResponseSchema);
    });
    it('response returns the correct email', () => {
      expect(response.data.email).eq('changeEmail@goshippo.com');
    });
    it('response returns the correct first_name', () => {
      expect(response.data.first_name).eq(body.first_name);
    });
    it('response returns the correct last_name', () => {
      expect(response.data.last_name).eq(body.last_name);
    });
    it('response returns the correct merchant_name', () => {
      expect(response.data.merchant_name).eq(body.merchant_name);
    });
  });
  describe('As a Shippo Partner I should be able to change my merchant first_name', () => {
    before('HTTP request to PUT /merchants/{{merchantId}}', async () => {
      const merchant = new MerchantService();
      body.first_name = 'John';
      const url = merchant.merchantUrl(merchantId);
      const h = headers();
      response = await httpRequest(url, 'PUT', h, body);
    });
    it('should return a 200 ok', () => {
      expect(response.status).eq(200);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsResponseSchema);
    });
    it('response returns the correct email', () => {
      expect(response.data.email).eq('changeEmail@goshippo.com');
    });
    it('response returns the correct first_name', () => {
      expect(response.data.first_name).eq('John');
    });
    it('response returns the correct last_name', () => {
      expect(response.data.last_name).eq(body.last_name);
    });
    it('response returns the correct merchant_name', () => {
      expect(response.data.merchant_name).eq(body.merchant_name);
    });
  });
  describe('As a Shippo Partner I should be able to change my merchant last_name', () => {
    before('HTTP request to PUT /merchants/{{merchantId}}', async () => {
      const merchant = new MerchantService();
      body.last_name = 'Smith';
      const url = merchant.merchantUrl(merchantId);
      const h = headers();
      response = await httpRequest(url, 'PUT', h, body);
    });
    it('should return a 200 ok', () => {
      expect(response.status).eq(200);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsResponseSchema);
    });
    it('response returns the correct email', () => {
      expect(response.data.email).eq('changeEmail@goshippo.com');
    });
    it('response returns the correct first_name', () => {
      expect(response.data.first_name).eq('John');
    });
    it('response returns the correct last_name', () => {
      expect(response.data.last_name).eq('Smith');
    });
    it('response returns the correct merchant_name', () => {
      expect(response.data.merchant_name).eq(body.merchant_name);
    });
  });
  describe('As a Shippo Partner I should be able to change my merchant merchant_name', () => {
    before('HTTP request to PUT /merchants/{{merchantId}}', async () => {
      const merchant = new MerchantService();
      body.merchant_name = 'Smith Shop';
      const url = merchant.merchantUrl(merchantId);
      const h = headers();
      response = await httpRequest(url, 'PUT', h, body);
    });
    it('should return a 200 ok', () => {
      expect(response.status).eq(200);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsResponseSchema);
    });
    it('response returns the correct email', () => {
      expect(response.data.email).eq('changeEmail@goshippo.com');
    });
    it('response returns the correct first_name', () => {
      expect(response.data.first_name).eq('John');
    });
    it('response returns the correct last_name', () => {
      expect(response.data.last_name).eq('Smith');
    });
    it('response returns the correct merchant_name', () => {
      expect(response.data.merchant_name).eq('Smith Shop');
    });
  });
  describe('As a Shippo Partner I should be able to retrieve the information related to a Merchant by its Id', () => {
    before('HTTP request to GET /merchants/{{merchantId}}', async () => {
      const merchant = new MerchantService();
      const url = merchant.merchantUrl(merchantId);
      const h = headers();
      response = await httpRequest(url, 'PUT', h, body);
    });
    it('should return a 200 ok', () => {
      expect(response.status).eq(200);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsResponseSchema);
    });
    it('response returns the correct email', () => {
      expect(response.data.email).eq('changeEmail@goshippo.com');
    });
    it('response returns the correct first_name', () => {
      expect(response.data.first_name).eq('John');
    });
    it('response returns the correct last_name', () => {
      expect(response.data.last_name).eq('Smith');
    });
    it('response returns the correct merchant_name', () => {
      expect(response.data.merchant_name).eq('Smith Shop');
    });
  });
  describe('As a Shippo Partner I should be able to retrieve the information related to all my Merchants', () => {
    before('HTTP request to GET /merchants', async () => {
      const merchant = new MerchantService();
      const url = merchant.merchantUrl();
      const h = headers();
      response = await httpRequest(url, 'GET', h);
    });
    it('should return a 200 ok', () => {
      expect(response.status).eq(200);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(AllmerchantsResponseSchema);
    });
    it('response should return an array of merchants', () => {
      expect(response.data.results.constructor).eq(Array);
    });
    it('response should contain a list of merchants', () => {
      expect(response.data.results.length > 0).to.be.true;
    });
  });
  describe('ADDRESSES', () => {
    let addressId: string = '';
    const bodyAddress = {
      city: 'San Francisco',
      company: 'Shippo',
      country: 'US',
      email: 'shippotle@goshippo.com',
      is_residential: true,
      metadata: 'Customer ID 123456',
      name: 'Shwan Ippotle',
      phone: '0015553419393',
      state: 'CA',
      street1: '215 Clayton St',
      street2: '',
      validate: true,
      zip: '94117-1913'
    };
    describe('As a Shippo Partner I should be able to set a valid address for a Merchants (request address validation)', () => {
      before(
        'HTTP request to POST /merchants/${merchantId}/addresses/',
        async () => {
          const merchant = new MerchantService();
          const url = merchant.addressUrl(merchantId);
          const h = headers(merchantId);
          response = await httpRequest(url, 'POST', h, bodyAddress);
          addressId = response.data.object_id;
        }
      );
      it('should return a 201 Created', () => {
        expect(response.status).eq(201);
      });
      it('should return the correct schema (ajv checks all types and structures)', () => {
        expect(response.data).to.be.jsonSchema(merchantsAddress);
      });
      it('response returns the correct name', () => {
        expect(response.data.name).eq(bodyAddress.name);
      });
      it('response returns the correct company', () => {
        expect(response.data.company).eq(bodyAddress.company);
      });
      it('response returns the correct city', () => {
        expect(response.data.city).eq(bodyAddress.city);
      });
      it('response returns the correct street1', () => {
        expect(response.data.street1).eq(bodyAddress.street1);
      });
      it('response returns the correct zip', () => {
        expect(response.data.zip).eq(bodyAddress.zip);
      });
      it('response returns the correct country', () => {
        expect(response.data.country).eq(bodyAddress.country);
      });
      it('response returns the correct phone', () => {
        expect(response.data.phone).eq(bodyAddress.phone);
      });
    });
    describe('As a Shippo Partner I should be able to set an invalid address for a Merchants by setting the validate property to false', () => {
      let res: AxiosResponse;
      const invalidBodyAddress = {
        city: 'The City of Angels',
        company: 'Shippo',
        country: 'US',
        email: 'shippotle@goshippo.com',
        is_residential: true,
        metadata: 'Customer ID 123456',
        name: 'Shwan Ippotle',
        phone: '0015553419393',
        state: 'CA',
        street1: '211 Address is so invalid',
        street2: '',
        validate: false,
        zip: '00000-00000'
      };
      before(
        'HTTP request to POST /merchants/${merchantId}/addresses/',
        async () => {
          const merchant = new MerchantService();
          const url = merchant.addressUrl(merchantId);
          const h = headers(merchantId);
          res = await httpRequest(url, 'POST', h, invalidBodyAddress);
        }
      );
      it('should return a 201 Created', () => {
        expect(res.status).eq(201);
      });
      it('should return the correct schema (ajv checks all types and structures)', () => {
        expect(response.data).to.be.jsonSchema(merchantsAddress);
      });
      it('response returns the correct name', () => {
        expect(res.data.name).eq(invalidBodyAddress.name);
      });
      it('response returns the correct company', () => {
        expect(res.data.company).eq(invalidBodyAddress.company);
      });
      it('response returns the correct city', () => {
        expect(res.data.city).eq(invalidBodyAddress.city);
      });
      it('response returns the correct street1', () => {
        expect(res.data.street1).eq(invalidBodyAddress.street1);
      });
      it('response returns the correct zip', () => {
        expect(res.data.zip).eq(invalidBodyAddress.zip);
      });
      it('response returns the correct country', () => {
        expect(res.data.country).eq(invalidBodyAddress.country);
      });
      it('response returns the correct phone', () => {
        expect(res.data.phone).eq(invalidBodyAddress.phone);
      });
    });
    describe('As a Shippo Partner I should be able to retrieve all addresses set for one of my Merchant', () => {
      before(
        'HTTP request to GET /merchants/${merchantId}/addresses/',
        async () => {
          const merchant = new MerchantService();
          const url = merchant.addressUrl(merchantId);
          const h = headers(merchantId);
          response = await httpRequest(url, 'GET', h);
        }
      );
      it('should return a 200 ok', () => {
        expect(response.status).eq(200);
      });
      it('should return the correct schema (ajv checks all types and structures)', () => {
        expect(response.data).to.be.jsonSchema(
          AllmerchantsAddressResponseSchema
        );
      });
      it('response should return an array of addresses', () => {
        expect(response.data.results.constructor).eq(Array);
      });
      it('response should contain a list of addresses', () => {
        expect(response.data.results.length > 0).to.be.true;
      });
      it('response should contain an invalid address ', () => {
        expect(
          response.data.results.filter(
            (a: any) => a.street1 === '211 Address is so invalid'
          )[0].street1
        ).eq('211 Address is so invalid');
      });
    });
    describe('As a Shippo Partner I should be able to retrieve a specific address for one of my Merchant', () => {
      before(
        'HTTP request to GET /merchants/${merchantId}/addresses/${addressId}',
        async () => {
          const merchant = new MerchantService();
          const url = merchant.addressUrl(merchantId);
          const h = headers(merchantId);
          response = await httpRequest(`${url}${addressId}`, 'GET', h);
        }
      );
      it('should return a 200 ok', () => {
        expect(response.status).eq(200);
      });
      it('should return the correct schema (ajv checks all types and structures)', () => {
        expect(response.data).to.be.jsonSchema(merchantsAddress);
      });
      it('response returns the correct name', () => {
        expect(response.data.name).eq(bodyAddress.name);
      });
      it('response returns the correct company', () => {
        expect(response.data.company).eq(bodyAddress.company);
      });
      it('response returns the correct city', () => {
        expect(response.data.city).eq(bodyAddress.city);
      });
      it('response returns the correct street1', () => {
        expect(response.data.street1).eq(bodyAddress.street1);
      });
      it('response returns the correct zip', () => {
        expect(response.data.zip).eq(bodyAddress.zip);
      });
      it('response returns the correct country', () => {
        expect(response.data.country).eq(bodyAddress.country);
      });
      it('response returns the correct phone', () => {
        expect(response.data.phone).eq(bodyAddress.phone);
      });
    });
    describe('As a Shippo Partner (Squarespace) I should be able to enable my merchants to change their previously saved address_from prior to purchasing UPS rate for a domestic shipment', () => {
      let res: AxiosResponse;
      const newBodyAddress = {
        city: 'San Jose',
        company: 'Shippo',
        country: 'US',
        email: 'shippotle@goshippo.com',
        is_residential: true,
        metadata: 'Customer ID 123456',
        name: 'Shwan Ippotle',
        phone: '0015553419393',
        state: 'CA',
        street1: '3610 Story Road',
        street2: '',
        validate: true,
        zip: '95127'
      };
      before(
        'HTTP request to POST  /merchants/${merchantId}/addresses/',
        async () => {
          const merchant = new MerchantService();
          const url = merchant.addressUrl(merchantId);
          const h = headers(merchantId);
          res = await httpRequest(url, 'POST', h, newBodyAddress);
        }
      );
      it('should return with a 201 Created', () => {
        expect(res.status).eq(201);
      });
      it('should return the correct schema (ajv checks all types and structures)', () => {
        expect(response.data).to.be.jsonSchema(merchantsAddress);
      });
      it('should return the new city', () => {
        expect(res.data.city).eq(newBodyAddress.city);
      });
      it('should return the new address', () => {
        expect(res.data.street1).eq('3610 Story Rd');
      });
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to enable my merchants to create their own custom packaging that can be used for an easy checkout', () => {
    before('HTTP request to /merchants/{{merchantId}}/parcels/', async () => {
      const parcel = new ParcelServices();
      response = await parcel.postCreateParcel(merchantId);
    });
    it('should return a 201 Created ok', () => {
      expect(response.status).eq(201);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsParcel);
    });
    it('response returns the correct distance_unit', () => {
      expect(response.data.distance_unit).eq('in');
    });
    it('response returns the correct template name', () => {
      expect(response.data.template).eq('USPS_LargeFlatRateBox');
    });
    it('response returns the correct insurance amount', () => {
      expect(response.data.extra.insurance.amount).eq('5.50');
    });
    it('response returns the correct insurance content', () => {
      expect(response.data.extra.insurance.content).eq('Laptop');
    });
  });
  describe('Preferred Currency Settings', () => {
    let carrierData: any;
    let responseMerchantRegistration: any;
    for (const currency of currencies) {
      // tslint:disable-next-line:forin
      for (const key in listOfCarriers) {
        const preferred_currency = currency.code;
        const country =
          listOfCarriers[key as keyof typeof listOfCarriers].country;
        const carrier =
          listOfCarriers[key as keyof typeof listOfCarriers].carrier;
        if (carrier !== 'ups') {
          describe(`As a Shippo Partner I should be able to create and register a merchant with ${carrier.toUpperCase()} and preferred_currency set to ${
            currency.code
          }`, () => {
            before('Merchant registration', async () => {
              const config: IConfig = {
                stateFrom: country,
                stateTo: country,
                carrier,
                preferred_currency,
                use_preferred_currency: true
              };
              carrierData = new CarrierRegistrationData(config);
              responseMerchantRegistration =
                await carrierData.registerCarrier();
            });

            it('should return a 201 Created', () => {
              if (responseMerchantRegistration.status === 201) {
                expect(responseMerchantRegistration.status).eq(201);
              } else {
                console.log(responseMerchantRegistration.data);
              }
            });
            it('should return the correct schema for merchant creation (ajv checks all types and structures)', () => {
              expect(carrierData.merchantResponse).to.be.jsonSchema(
                merchantsResponseSchema
              );
            });
            it('should return the correct schema for merchant registration (ajv checks all types and structures)', () => {
              expect(responseMerchantRegistration.data).to.be.jsonSchema(
                merchantRegistrationSchema
              );
            });
            it(`response returns the correct preferred currency as ${currency.code}`, () => {
              expect(carrierData.merchantResponse.preferred_currency).eq(
                preferred_currency
              );
            });
          });
        }
      }
    }
  });
});
