import { expect } from '@utils/common.util';
import { AxiosResponse } from 'axios';
import { TransactionsData } from '@data/transactions.data';
import { IConfig } from '@utils/common.util';
import { merchantsErrorSchema } from '@schemas/merchants-schema';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Transactions API - Negative Flows`, () => {
  let response: AxiosResponse;
  describe('As a Shippo Partner (Squarespace) I should NOT be able to create a transaction if name is missing from the address_from or address_to (Shipment creation)', () => {
    before('Create Transaction', async () => {
      const config: IConfig = {
        isPositiveFlow: false,
        carrier: 'ups'
      };
      const t = new TransactionsData(config);
      response = await t.createATransactionWithTheMissingProperty('name');
    });
    it('should NOT create a transaction - 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
  });
  describe('As a Shippo Partner (Squarespace) I should NOT be able to create a transaction if the rate is either missing or empty', () => {
    const config: IConfig = {
      isPositiveFlow: false,
      carrier: 'ups'
    };
    const t = new TransactionsData(config);
    before('Attempt to Create Transaction', async () => {
      response = await t.attemptToCreateATransactionNoMerchantRate();
    });
    it('should NOT create a transaction - 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(merchantsErrorSchema);
    });
  });
  describe("As a Shippo Partner (Squarespace) I should NOT be able to retrieve a transaction details by and incorrect Id or an Id related to a different merchant's transaction", () => {
    let getTransactionResponse: AxiosResponse;
    let t: any;
    before('Create Transaction', async () => {
      const config: IConfig = {
        isPositiveFlow: false,
        carrier: 'ups'
      };
      t = new TransactionsData(config);
      response = await t.createATransaction();
    });
    before('Retrieve a Transaction by Id', async () => {
      getTransactionResponse = await t.getTransactionById(
        'dhdshjdsd8s98d9s8d9s89d8s9'
      );
    });
    it('should NOT create a transaction - 400 Bad Request', () => {
      expect(getTransactionResponse.status).eq(404);
    });
    it.skip('should return the correct schema (ajv checks all types and structures)', () => {
      expect(getTransactionResponse.data).to.be.jsonSchema(
        merchantsErrorSchema
      );
    });
    // TODO waiting defect resolution
    it.skip('should return  the relevant error message', () => {
      expect(getTransactionResponse.data.transactions[0]).eq(
        'The requested URL /transactions/notrealTransactionId was not found on this server'
      );
    });
  });
});
