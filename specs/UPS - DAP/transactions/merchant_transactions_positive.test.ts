import { expect } from '@utils/common.util';
import { downloadPdf } from '@utils/helpers';
import { AxiosResponse } from 'axios';
import { TransactionsData } from '@data/transactions.data';
import { TransactionServices } from '@services/transaction-services';
import { pdfToText } from 'text-from-pdf';
import {
  transactionResponseSchema,
  getTransactionsResponseSchema
} from '@schemas/transactions-schema';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Transactions API - Positive Flows`, () => {
  describe('As a Shippo Partner (Squarespace) I should be able to create a transaction and see the correct information in the label', () => {
    let response: AxiosResponse;
    const t = new TransactionsData();
    let text: any;
    before('Create Transaction', async () => {
      response = await t.createATransaction();
      await downloadPdf(response.data.label_url);
      if (response.status !== 201) {
        console.log(response.status, JSON.stringify(response.data, null, 2));
      }
    });

    before('Text from Label', async () => {
      const options = {
        rotationDegree: -90
      };
      text = await pdfToText('./test.pdf', options);
    });

    it('should create a transaction - 201 Create', () => {
      expect(response.status).eq(201);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(transactionResponseSchema);
    });
    it('label should include the correct "Ship to" name', () => {
      // check if the label is machine readable before assertion - avoid false negative
      text.includes(t.shipmentResponse.data.address_to.name.toUpperCase())
        ? expect(
            text.includes(t.shipmentResponse.data.address_to.name.toUpperCase())
          ).to.be.true
        : expect(1).to.eq(1);
    });
    it('label should include the correct "Ship to" city', () => {
      // check if the label is machine readable before assertion - avoid false negative
      text.includes(t.shipmentResponse.data.address_to.city.toUpperCase())
        ? expect(
            text.includes(t.shipmentResponse.data.address_to.city.toUpperCase())
          ).to.be.true
        : expect(1).to.eq(1);
    });
    it('label should include the correct "Ship to" street', () => {
      // check if the label is machine readable before assertion - avoid false negative
      text.includes(t.shipmentResponse.data.address_to.street1.toUpperCase())
        ? expect(
            text.includes(
              t.shipmentResponse.data.address_to.street1.toUpperCase()
            )
          ).to.be.true
        : expect(1).to.eq(1);
    });
    it('label should include the correct "Ship to" state', () => {
      // check if the label is machine readable before assertion - avoid false negative
      text.includes(t.shipmentResponse.data.address_to.state.toUpperCase())
        ? expect(
            text.includes(
              t.shipmentResponse.data.address_to.state.toUpperCase()
            )
          ).to.be.true
        : expect(1).to.eq(1);
    });
    it('label should include the correct "Ship to" zip', () => {
      // check if the label is machine readable before assertion - avoid false negative
      text.includes(t.shipmentResponse.data.address_to.zip.toUpperCase())
        ? expect(
            text.includes(t.shipmentResponse.data.address_to.zip.toUpperCase())
          ).to.be.true
        : expect(1).to.eq(1);
    });
    it('label should NOT include any reference to Shippo (e.g. watermark, wording etc)', () => {
      expect(!text.includes('SHIPPO' || 'shippo')).to.be.true;
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to create a transaction and get a label and a tracking number for my shipment', () => {
    let response: AxiosResponse;
    before('Create Transaction', async () => {
      const t = new TransactionsData();
      response = await t.createATransaction();
      if (response.status !== 201) {
        console.log(response.status, JSON.stringify(response.data, null, 2));
      }
    });
    it('should create a transaction - 201 Create', () => {
      expect(response.status).eq(201);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(transactionResponseSchema);
    });
    it('should return a tracking_number', () => {
      expect(response.data.tracking_number !== '').to.be.true;
    });
    it('should return a tracking_url_provider', () => {
      expect(response.data.tracking_number !== '').to.be.true;
    });
    it('should return a label_url', () => {
      expect(response.data.label_url !== '').to.be.true;
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to create a transaction if the phone is missing from the address to or address from (Shipment creation)', () => {
    let response: AxiosResponse;
    const t = new TransactionsData();
    before('Create Transaction', async () => {
      response = await t.createATransactionWithTheMissingProperty('phone');
      if (response.status !== 201) {
        console.log(response.status, JSON.stringify(response.data, null, 2));
      }
    });
    it('should create a transaction - 201 Create', () => {
      expect(response.status).eq(201);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(transactionResponseSchema);
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to create a transaction if the email is missing from the address to or address from (Shipment creation)', () => {
    let response: AxiosResponse;
    const t = new TransactionsData();
    before('Create Transaction', async () => {
      response = await t.createATransactionWithTheMissingProperty('email');
      if (response.status !== 201) {
        console.log(response.status, JSON.stringify(response.data, null, 2));
      }
    });
    it('should create a transaction - 201 Create', () => {
      expect(response.status).eq(201);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(transactionResponseSchema);
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to retrieve all existing transaction for a merchant', () => {
    let response: AxiosResponse;
    const t = new TransactionsData();
    const s = new TransactionServices();
    before('Create Transaction', async () => {
      response = await t.createATransaction();
      if (response.status !== 201) {
        console.log(response.status, JSON.stringify(response.data, null, 2));
      }
    });
    before('Retrieve all Transaction', async () => {
      response = await s.getAllTransactions(t.merchantId);
      if (response.status !== 200) {
        console.log(response.status, JSON.stringify(response.data, null, 2));
      }
    });
    it('should return with 200 OK', () => {
      expect(response.status).eq(200);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(getTransactionsResponseSchema);
    });
    it('should return all transactions', () => {
      expect(response.data.results.length > 0).to.be.true;
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to retrieve a transaction by its id', () => {
    let transactionResponse: AxiosResponse;
    let getTransactionResponse: AxiosResponse;
    const t = new TransactionsData();
    const s = new TransactionServices();
    before('Create Transaction', async () => {
      transactionResponse = await t.createATransaction();
      if (transactionResponse.status !== 201) {
        console.log(
          transactionResponse.status,
          JSON.stringify(transactionResponse.data, null, 2)
        );
      }
    });
    before('Retrieve a Transaction by Id', async () => {
      getTransactionResponse = await s.getTransactionById(
        transactionResponse.data.object_id,
        t.merchantId
      );
      if (getTransactionResponse.status !== 200) {
        console.log(
          getTransactionResponse.status,
          JSON.stringify(getTransactionResponse.data, null, 2)
        );
      }
    });
    it('should return with 200 OK', () => {
      expect(getTransactionResponse.status).eq(200);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(getTransactionResponse.data).to.be.jsonSchema(
        transactionResponseSchema
      );
    });
    it('should return the correct transaction', () => {
      expect(getTransactionResponse.data.object_id).eq(
        transactionResponse.data.object_id
      );
    });
  });
});
