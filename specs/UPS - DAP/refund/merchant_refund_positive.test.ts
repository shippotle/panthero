import { expect } from '@utils/common.util';
import { AxiosResponse } from 'axios';

import { RefundData } from '@data/refund.data';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Refund API - Positive Flows`, () => {
  let response: AxiosResponse;
  describe('As a Shippo Partner (Squarespace) I should be able to refund a transaction', () => {
    const refund = new RefundData();
    before('Create Refund', async () => {
      response = await refund.createARefund();
    });

    it('should return a 201 Created', () => {
      expect(response.status).eq(201);
    });
    it('should return with status = SUCCESS', () => {
      const message = process.env.TOKEN === 'TEST' ? 'SUCCESS' : 'PENDING';
      expect(response.data.status).eq(message);
    });
    it('should return with right transactionId', () => {
      expect(response.data.transaction).eq(refund.transactionId);
    });
  });
});
