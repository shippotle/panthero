import { expect } from '@utils/common.util';
import { httpRequest } from '@utils/helpers';
import { AxiosResponse } from 'axios';
import { RefundData } from '@data/refund.data';
import { refundsErrorSchema } from '@schemas/refund-schema';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Refund API - Negative Flows`, () => {
  let response: AxiosResponse;
  describe('As a Shippo Partner (Squarespace) I should NOT be able to refund a transaction if the transactionId is missing or empty', () => {
    before('Create a Refund', async () => {
      const config = {
        isPositiveFlow: false,
        carrier: 'ups'
      };
      const refund = new RefundData(config);
      response = await refund.createARefundWithNoTransactionId();
    });

    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(refundsErrorSchema);
    });
    it('should return with the correct error message', () => {
      expect(response.data.transaction[0]).eq('This field is required.');
    });
  });
  describe("As a Shippo Partner (Squarespace) I should NOT be able to refund a transaction if the transactionId is invalid or associated with a different merchantId's transaction", () => {
    before('Create a Refund', async () => {
      const config = {
        isPositiveFlow: false,
        carrier: 'ups'
      };
      const refund = new RefundData(config);
      response = await refund.createARefund('dhhahhhd29934992kkdd');
    });
    it('should return a 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(refundsErrorSchema);
    });
    it('should return with the correct error message', () => {
      expect(response.data.transaction[0]).eq(
        'Transaction with supplied object_id not found.'
      );
    });
  });
  describe('As a Shippo Partner (Squarespace) I should NOT create a duplicate for an existing and already refunded transaction', () => {
    let responseSecondRefund: AxiosResponse;
    before('Create first Refund', async () => {
      const config = {
        isPositveFlow: false,
        carrier: 'ups'
      };
      const r = new RefundData(config);
      await r.createARefund();
      responseSecondRefund = await httpRequest(
        r.refund.refundUrl(r.merchantId),
        'POST',
        r.refund.headers(r.merchantId),
        {
          transaction: r.transactionId,
          async: false
        }
      );
    });

    it('should return a 400 Bad Request', () => {
      expect(responseSecondRefund.status).eq(400);
    });
    it('should return the correct schema (ajv checks all types and structures)', () => {
      expect(response.data).to.be.jsonSchema(refundsErrorSchema);
    });
    it('should return with the correct error message', () => {
      expect(responseSecondRefund.data.transaction[0]).eq(
        'Refund with this Transaction already exists.'
      );
    });
  });
});
