import { expect } from '@utils/common.util';
import { AxiosResponse } from 'axios';
import { TransactionsData } from '@data/transactions.data';
const combinations = require('@data/combinations.json');
import { pickMultipleRandomElementFromAList } from '@utils/helpers';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} -Complete Transactions - Combinations of random addresses between different US states`, () => {
  for (const combination of pickMultipleRandomElementFromAList(
    combinations,
    53
  )) {
    describe(`As a Shippo Partner (Squarespace) I should be able to enable my merchants create transactions between addresses ${combination.first} and ${combination.second}`, () => {
      let response: AxiosResponse;
      before('Create a Shipment', async () => {
        const config = {
          stateFrom: `${combination.first}`,
          stateTo: `${combination.second}`,
          carrier: 'ups',
          use_preferred_currency: false
        };
        const transaction = new TransactionsData(config);
        response = await transaction.createATransaction();
      });
      it('should return with a label url', () => {
        expect(response.data.label_url !== 0).to.be.true;
      });
    });
  }
});
