import { expect } from '@utils/common.util';
import { AxiosResponse } from 'axios';
import { RefundServices } from '@services/refund-services';
import { TransactionsData } from '@data/transactions.data';

const territories = [
  'Guam',
  'Puerto-Rico',
  'Virgin-Islands',
  // 'American-Samoa',
  'Northern-Mariana-Islands'
];

const carrier = 'ups';
describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - ${carrier.toUpperCase()} - E2E Complete Business Transactions Flows`, () => {
  describe('As a Shippo Partner (Squarespace) I should be able to enable my merchants to chose and purchase a UPS rate for a shipping between two US residential addresses within the same city (Intra-State / same city)', () => {
    let response: AxiosResponse;
    before('Create a Shipment', async () => {
      const config = {
        stateFrom: 'AZ',
        stateTo: 'AZ',
        sameCity: true,
        carrier
      };
      const transaction = new TransactionsData(config);
      response = await transaction.createATransaction();
    });
    it('should return with a label url', () => {
      expect(response.data.label_url !== '').to.be.true;
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to enable my merchants to chose and purchase a UPS rate for a shipping between two cities within the same US state (Intra-State / different cities)', () => {
    let response: AxiosResponse;
    before('Create a Shipment', async () => {
      const config = {
        stateFrom: 'AZ',
        stateTo: 'AZ',
        sameCity: false,
        carrier
      };
      const transaction = new TransactionsData(config);
      response = await transaction.createATransaction();
    });
    it('should return with a label url', () => {
      expect(response.data.label_url !== '').to.be.true;
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to enable my merchants to chose and purchase a UPS shipping rate for a shipping between two cities from two different US states (Inter-State)', () => {
    let response: AxiosResponse;
    const config = {
      stateFrom: 'CT',
      stateTo: 'CO',
      carrier
    };
    before('Create a Shipment', async () => {
      const transaction = new TransactionsData(config);
      response = await transaction.createATransaction();
    });
    it('should return with a label url', () => {
      expect(response.data.label_url !== '').to.be.true;
    });
  });
  describe('International', () => {
    let transactionId: string = '';
    let merchantId: string = '';
    describe('As a Shippo Partner (Squarespace) I should be able to enable my merchants to create a custom declaration for International shipping - US Export (International UK)', () => {
      let response: AxiosResponse;
      before('Create a Shipment', async () => {
        const config = {
          stateFrom: 'KY',
          stateTo: 'GB-UK',
          carrier
        };
        const transaction = new TransactionsData(config);
        response = await transaction.createInternationalTransaction();
      });
      it('should return with a label url', () => {
        expect(response.data.label_url !== '').to.be.true;
      });
    });
    describe('As a Shippo Partner (Squarespace) I should be able to enable my merchants to create a custom declaration for International shipping - US Export (International Canada)', () => {
      let response: AxiosResponse;
      before('Create a Shipment', async () => {
        const config = {
          stateFrom: 'MD',
          stateTo: 'CAN',
          carrier
        };
        const transaction = new TransactionsData(config);
        response = await transaction.createInternationalTransaction();
        transactionId = response.data.object_id;
        merchantId = transaction.merchantId;
      });
      it('should return with a label url', () => {
        expect(response.data.label_url !== '').to.be.true;
      });
    });
    describe('As a Shippo Partner (Squarespace) I should be able to refund an International transaction', () => {
      let response: AxiosResponse;
      const refund = new RefundServices();
      before('Refund International a Shipment', async () => {
        response = await refund.postRefund(transactionId, merchantId);
      });
      it('should return a 201 Created', () => {
        expect(response.status).eq(201);
      });
      it('should return with the correct status (SUCCESS or PENDING)', () => {
        const message = process.env.TOKEN === 'TEST' ? 'SUCCESS' : 'PENDING';
        expect(response.data.status).eq(message);
      });
      it('should return with right transactionId', () => {
        expect(response.data.transaction).eq(transactionId);
      });
    });
  });
  describe('US Territories and Possessions', () => {
    for (const territory of territories) {
      describe(`As a Shippo Partner (Squarespace) I should be able to enable my merchants to ship to the US Territory of ${territory
        .split('-')
        .join(' ')}`, () => {
        let response: AxiosResponse;
        before('Create a Shipment', async () => {
          const config = {
            stateFrom: 'CA',
            stateTo: `${territory}`,
            carrier
          };
          const transaction = new TransactionsData(config);
          response = await transaction.createInternationalTransaction();
        });
        it('should return with a label url', () => {
          expect(response.data.label_url !== '').to.be.true;
        });
      });
    }
  });
  describe('As a Shippo Partner (Squarespace) I should be able to enable my merchants to track their domestic shipment (merchant can subscribe to a webhook)', () => {
    let response: AxiosResponse;
    before('Create a Transaction with webhook subscription', async () => {
      const config = {
        stateFrom: 'CA',
        stateTo: 'AR',
        carrier
      };
      const transaction = new TransactionsData(config);
      response = await transaction.trackTheStatusForOrder();
    });
    it('should return with a 200 OK', () => {
      // this is mocked and will work on in QA - prod needs a real tracking number hence why not possible to assert
      if (process.env.TOKEN === 'TEST') {
        expect(response.status).eq(200);
      } else expect(response.status).eq(401);
    });
  });
});

describe.skip(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Testing UPS carrier registration for real`, () => {
  let response: AxiosResponse;
  before('Create a Shipment', async () => {
    const config = {
      stateFrom: 'AZ',
      stateTo: 'AZ',
      carrier
    };
    const transaction = new TransactionsData(config);
    response = await transaction.createATransaction();
  });
  it('should return with a label url', () => {
    expect(response.data.label_url !== '').to.be.true;
  });
});
