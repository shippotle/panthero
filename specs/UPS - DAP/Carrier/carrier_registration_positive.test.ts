import { expect, listOfCarriers, IConfig } from '@utils/common.util';
import { MerchantService } from '@services/merchant-services';
import { AxiosResponse } from 'axios';
import { CarrierRegistrationData } from '@data/carrier-registration.data';

describe(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Carrier API - Positive Flows`, () => {
  // UPS registration flow is mocked as we are not allowed to create multiple merchants
  const config: IConfig = {
    carrier: listOfCarriers.ups.carrier
  };
  describe('As a Shippo Partner (Squarespace) I should be able to register my merchant with UPS as carrier', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier();
    });
    it('ups has been registered - 201 Created', () => {
      expect(response.status).eq(201);
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to register my merchants to use UPS as carrier omitting the company property', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier('company');
    });
    it('ups has been registered - 201 Created', () => {
      expect(response.status).eq(201);
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to register my merchants to use UPS as carrier omitting the email property', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier('email');
    });
    it('ups has been registered - 201 Created', () => {
      expect(response.status).eq(201);
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to register my merchants to use UPS as carrier omitting the phone', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier('phone');
    });
    it('ups has been registered - 201 Created', () => {
      expect(response.status).eq(201);
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to register my merchants to use UPS as carrier omitting the pickup_address_same_as_billing_address', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier(
        'pickup_address_same_as_billing_address'
      );
    });
    it('ups has been registered - 201 Created', () => {
      expect(response.status).eq(201);
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be able to register my merchants to use UPS as carrier by setting the pickup_address_same_as_billing_address to true and all remaining "pickup_address_XXXX" properties are empty of missing', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrierWithPickSameAsBilling();
    });

    it('ups has been registered - 201 Created', () => {
      expect(response.status).eq(201);
    });
  });
  describe("As a Shippo Partner (Squarespace) I should be able to check the status of my merchant's carrier registration", () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      await carrierData.registerCarrier();
      response = await carrierData.merchant.getCarrierRegistrationStatus(
        carrierData.merchantId
      );
      if (response.status !== 200) {
        console.log(response.status, JSON.stringify(response.data, null, 2));
      }
    });
    it('ups has been registered - 200 OK', () => {
      expect(response.status).eq(200);
    });
    it("should return carrier registration' status = complete", () => {
      expect(response.data.results[0].status).eq('COMPLETE');
    });
  });
});
