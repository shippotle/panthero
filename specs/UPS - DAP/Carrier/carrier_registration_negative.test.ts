import { expect, IConfig } from '@utils/common.util';
import { AxiosResponse } from 'axios';
import { CarrierRegistrationData } from '@data/carrier-registration.data';

describe.skip(`${
  process.env.isPlatformUrl === 'true' ? 'Platform API' : 'Public API'
} - Carrier API - Negative Flows`, () => {
  const config: IConfig = {
    isPositiveFlow: false
  };
  describe('As a Shippo Partner (Squarespace) I should be NOT be able to register my merchants to use UPS as carrier if ups_agreements is set to false (not agreeing to UPS terms and conditions)', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier('', false);
    });
    it('ups should NOT be registered - 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return with a relevant error message', () => {
      expect(response.data.error).eq(
        'User did not agree to UPS Technology Agreement'
      );
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be NOT be able to register my merchants to use UPS as carrier if ups_agreements is missing as a property', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier('ups_agreements', false);
    });
    it('ups should NOT be registered - 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return with a relevant error message', () => {
      expect(response.data.message[0]).eq(
        "The following parameters are required to register an account with UPS: ['ups_agreements']"
      );
    });
    it('should return with a relevant error code', () => {
      expect(response.data.error_code[0]).eq('account_missing_params');
    });
    it('should indicate the relevant missing parameter', () => {
      expect(response.data.params[0]).eq('ups_agreements');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be NOT be able to register my merchants to use UPS if the "carrier" property is empty', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier('carrier', false);
    });
    it('ups should NOT be registered - 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return with a relevant error message', () => {
      expect(response.data.carrier[0]).eq('This field is required.');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be NOT be able to register my merchants to use UPS if the "billing_address_city" property is empty or missing', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier(
        'billing_address_city',
        false
      );
    });
    it('ups should NOT be registered - 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return with a relevant error message', () => {
      expect(response.data.message[0]).eq(
        "The following parameters are required to register an account with UPS: ['billing_address_city']"
      );
    });
    it('should return with a relevant error code', () => {
      expect(response.data.error_code[0]).eq('account_missing_params');
    });
    it('should indicate the relevant missing parameter', () => {
      expect(response.data.params[0]).eq('billing_address_city');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be NOT be able to register my merchants to use UPS if the "billing_address_country_iso2" property is empty or missing', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier(
        'billing_address_country_iso2',
        false
      );
    });
    it('ups should NOT be registered - 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return with a relevant error message', () => {
      expect(response.data.message[0]).eq(
        "The following parameters are required to register an account with UPS: ['billing_address_country_iso2']"
      );
    });
    it('should return with a relevant error code', () => {
      expect(response.data.error_code[0]).eq('account_missing_params');
    });
    it('should indicate the relevant missing parameter', () => {
      expect(response.data.params[0]).eq('billing_address_country_iso2');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be NOT be able to register my merchants to use UPS if the "billing_address_state" property is empty or set to an invalid value', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier(
        'billing_address_state',
        false
      );
    });
    it('ups should NOT be registered - 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return with a relevant error message', () => {
      expect(response.data.message[0]).eq(
        "The following parameters are required to register an account with UPS: ['billing_address_state']"
      );
    });
    it('should return with a relevant error code', () => {
      expect(response.data.error_code[0]).eq('account_missing_params');
    });
    it('should indicate the relevant missing parameter', () => {
      expect(response.data.params[0]).eq('billing_address_state');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be NOT be able to register my merchants to use UPS if the "billing_address_street1" property is empty empty or missing', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier(
        'billing_address_street1',
        false
      );
    });
    it('ups should NOT be registered - 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return with a relevant error message', () => {
      expect(response.data.message[0]).eq(
        "The following parameters are required to register an account with UPS: ['billing_address_street1']"
      );
    });
    it('should return with a relevant error code', () => {
      expect(response.data.error_code[0]).eq('account_missing_params');
    });
    it('should indicate the relevant missing parameter', () => {
      expect(response.data.params[0]).eq('billing_address_street1');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be NOT be able to register my merchants to use UPS if the "billing_address_zip " property is empty empty or missing', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData(config);
      response = await carrierData.registerCarrier(
        'billing_address_zip',
        false
      );
    });
    it('ups should NOT be registered - 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return with a relevant error message', () => {
      expect(response.data.message[0]).eq(
        "The following parameters are required to register an account with UPS: ['billing_address_zip']"
      );
    });
    it('should return with a relevant error code', () => {
      expect(response.data.error_code[0]).eq('account_missing_params');
    });
    it('should indicate the relevant missing parameter', () => {
      expect(response.data.params[0]).eq('billing_address_zip');
    });
  });
  describe('As a Shippo Partner (Squarespace) I should be NOT be able to register my merchants to use UPS if the "pickup_address_country_iso2" property is empty or missing and pickup_address_same_as_billing_address" is set to false', () => {
    let response: AxiosResponse;
    before('Register UPS as Carrier for Squarespace Merchant', async () => {
      const carrierData = new CarrierRegistrationData();
      response = await carrierData.attemptToRegisterACarrierWithMissingProp(
        'pickup_address_country_iso2',
        false
      );
    });
    it('ups should NOT be registered - 400 Bad Request', () => {
      expect(response.status).eq(400);
    });
    it('should return with a relevant error message', () => {
      expect(response.data.message[0]).eq(
        "The following parameters are required to register an account with UPS: ['pickup_address_country_iso2']"
      );
    });
    it('should return with a relevant error code', () => {
      expect(response.data.error_code[0]).eq('account_missing_params');
    });
    it('should indicate the relevant missing parameter', () => {
      expect(response.data.params[0]).eq('pickup_address_country_iso2');
    });
  });
});
