import { AxiosResponse } from 'axios';
import { MerchantRegistrationData } from '@data/merchant-registration.data';
import { ShipmentService } from '@services/shipment-service';
import { carrierRegisteredWithUPS } from '@data/carrier-registered-merchants';
import { pickRandomElementFromList } from '@utils/helpers';
import { IConfig, defaultConfig } from '@utils/common.util';
import { MerchantService } from '@services/merchant-services';

export class ShipmentData {
  public merchantId: string = '';
  merchant: any;
  shipment: any;
  config: IConfig;
  merchantService: any;
  constructor(config?: IConfig) {
    this.config = {
      ...defaultConfig,
      ...config
    };
    this.shipment = new ShipmentService(this.config);
  }

  async carrierRegistration(
    config: IConfig = this.config,
    merchantId?: string
  ) {
    let registrationResponse;
    if (config.carrier !== 'ups') {
      this.merchant = new MerchantRegistrationData(config);
      this.merchantId = merchantId ?? (await this.merchant.merchantCreation());
      registrationResponse = await this.merchant.merchantRegistration(
        config,
        this.merchantId
      );
    } else {
      this.merchantId =
        merchantId ?? pickRandomElementFromList(carrierRegisteredWithUPS)[0];
      this.merchantService = new MerchantService(config);
      registrationResponse = await this.merchantService.putAlterMerchant(
        this.merchantId,
        {
          preferred_currency: this.config.preferred_currency,
          use_preferred_currency: this.config.use_preferred_currency
        }
      );
    }
    return registrationResponse;
  }

  async createAShipment(config?: IConfig, merchantId?: string) {
    await this.carrierRegistration(config);
    const shipmentBody = this.shipment.createShipment(
      config ? config : this.config
    );
    const shipmentResponse = await this.shipment.postShipment(
      shipmentBody,
      merchantId ?? this.merchantId
    );
    return shipmentResponse;
  }

  async createAShipmentWithAddOn(addOn: any) {
    await this.carrierRegistration();
    const shipmentBody = this.shipment.createShipment();

    Object.assign(shipmentBody, addOn);

    const shipmentResponse = await this.shipment.postShipment(
      shipmentBody,
      this.merchantId
    );

    return shipmentResponse;
  }

  async createAShipmentWithoutACarrierRegistration(
    isWithoutCarrierRegistration?: boolean
  ) {
    this.merchant = new MerchantRegistrationData();
    this.merchantId = await this.merchant.merchantCreation();

    const shipmentBody = this.shipment.createShipment();

    const shipmentResponse = await this.shipment.postShipment(
      shipmentBody,
      this.merchantId,
      isWithoutCarrierRegistration
    );

    return shipmentResponse;
  }

  async createAShipmentWithAMissingProperty(
    properties: string | string[]
  ): Promise<AxiosResponse> {
    await this.carrierRegistration();
    const shipmentBody = this.shipment.createShipment();
    if (
      !Array.isArray(properties) &&
      (properties === 'address_from' ||
        properties === 'address_to' ||
        properties === 'parcels')
    ) {
      delete shipmentBody[properties];
    } else if (typeof properties === 'string') {
      delete shipmentBody.address_from[properties];
      delete shipmentBody.address_to[properties];
    } else
      for (const property of properties) {
        delete shipmentBody.address_from[property];
        delete shipmentBody.address_to[property];
      }

    const shipmentResponse = await this.shipment.postShipment(
      shipmentBody,
      this.merchantId
    );
    return shipmentResponse;
  }

  async createAnInternationalShipment(
    config?: IConfig,
    merchantId?: string
  ): Promise<AxiosResponse> {
    const responseFromCarrierReg = await this.carrierRegistration(
      config,
      merchantId
    );

    if (config?.carrier === 'ups') {
      console.log(
        'Customer Registration Response:',
        responseFromCarrierReg.data
      );
    }

    const shipmentBody = this.shipment.createShipment(config);

    const cDcl = await this.shipment.customDeclaration(this.merchantId, config);
    const customs_declaration = cDcl.data.object_id;

    Object.assign(shipmentBody, { customs_declaration });
    const shipmentResponse = await this.shipment.postShipment(
      shipmentBody,
      this.merchantId
    );
    return shipmentResponse;
  }
}
