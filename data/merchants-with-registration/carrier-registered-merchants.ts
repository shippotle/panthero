import { env } from '@utils/common.util';

export const carrierRegisteredWithUPS =
  env === 'qa'
    ? [
        '6b3b940cf2684eb094f744b760abbcb9',
        '5c7b8136c14f48cb8faed399665eed29',
        '6cf16d49e757445e8b35688fa2d93204',
        'e4b7927372684f169b9f2b1a47994812',
        '1137b5a945f9452fa14fa5abba565ae0',
        'aa660169efd34a1fbb0d9931d337f344',
        '3b92dfaa606840938160d8cdadbf1d3f',
        '4369fbdcd9904766b466cf89a451a5e7',
        '388281b1ce7441f097dfb3f0896d056b',
        '58259463d6ef407e87ffb33cd7da445c',
        '25eea31b984641daac323a21014c41f1',
        '3bf98bb0f4054026865d8965baa22195',
        '889141b5213643cf99b06103ab7cdb7c'
      ]
    : ['72192993693f43c9ba6f074e68c7a764'];
