import { TransactionsData } from '@data/transactions.data';
import { AxiosResponse } from 'axios';
import { RefundServices } from '@services/refund-services';
import { IConfig, defaultConfig } from '@utils/common.util';

export class RefundData {
  transaction: any;
  refund: any;
  config: IConfig;
  public transactionId: string = '';
  public merchantId: string = '';

  constructor(config?: IConfig) {
    this.config = {
      ...defaultConfig,
      ...config
    };
    this.transaction = new TransactionsData(this.config);
    this.refund = new RefundServices(this.config);
  }

  async createARefund(transactionId?: string): Promise<AxiosResponse> {
    await this.transaction.createATransaction();
    this.merchantId = this.transaction.merchantId;
    const trId = transactionId ? transactionId : this.transaction.transactionId;
    const refundResponse = await this.refund.postRefund(
      trId,
      this.transaction.merchantId
    );
    this.transactionId = this.transaction.transactionId;
    return refundResponse;
  }
  async createARefundWithNoTransactionId(): Promise<AxiosResponse> {
    await this.transaction.createATransaction();
    this.merchantId = this.transaction.merchantId;
    const refundResponse = await this.refund.postRefund(
      this.transaction.transactionId,
      this.transaction.merchantId,
      true
    );
    this.transactionId = this.transaction.transactionId;
    return refundResponse;
  }
}
