import { MerchantService, IAddress } from '@services/merchant-services';
import { pickRandomElementFromList } from '@utils/helpers';
import { carrierRegisteredWithUPS } from '@data/carrier-registered-merchants';

import { IConfig, defaultConfig } from '@utils/common.util';
export class CarrierRegistrationData {
  public merchantId: string = '';
  public merchantResponse = {};
  config: IConfig;
  merchant;
  registerCarrierBody;
  constructor(config?: IConfig) {
    this.config = {
      ...defaultConfig,
      ...config
    };
    this.merchant = new MerchantService(config);
    this.registerCarrierBody = this.merchant.returnCarrierBody(
      this.config.carrier
    );
  }

  async registerCarrier(
    removeProperty?: string,
    agreement = true
  ): Promise<any> {
    if (this.config.carrier !== 'ups') {
      const m = await this.merchant.postCreateNewMerchant();
      this.merchantResponse = m.data;
      this.merchantId = m.data.object_id;

      if (!agreement) {
        // @ts-ignore
        this.registerCarrierBody.parameters.ups_agreements = false;
      }

      if (
        removeProperty &&
        removeProperty !== 'carrier' &&
        this.registerCarrierBody.parameters.hasOwnProperty(removeProperty)
      ) {
        // @ts-ignore
        delete this.registerCarrierBody.parameters[removeProperty];
      } else {
        // @ts-ignore
        delete this.registerCarrierBody[removeProperty];
      }
      return this.merchant.postMerchantRegistration(
        this.registerCarrierBody,
        this.config,
        this.merchantId
      );
    } else {
      this.merchantId = pickRandomElementFromList(carrierRegisteredWithUPS)[0];
      const merchantCreationResponse = await this.merchant.putAlterMerchant(
        this.merchantId,
        {
          preferred_currency: this.config.preferred_currency,
          use_preferred_currency: this.config.use_preferred_currency
        }
      );
      this.merchantResponse = merchantCreationResponse.data;

      return {
        status: 201,
        data: {
          carrier: 'ups',
          object_id: 'cbec0b237a4c4e229d8f78e1ad1ab74e',
          object_owner: '199_c5b9d843-edf2-455f-9115-f631ee309e26',
          account_id: 'shippo_ups_account',
          parameters: {},
          test: false,
          active: true,
          is_shippo_account: true,
          metadata: '',
          carrier_name: 'UPS',
          carrier_images: {
            200: 'https://dev-qa-static-shippodev-com.s3.amazonaws.com/providers/200/UPS.png',
            75: 'https://dev-qa-static-shippodev-com.s3.amazonaws.com/providers/75/UPS.png'
          }
        }
      };
    }
  }

  async registerCarrierWithPickSameAsBilling(
    isPositiveFlow?: boolean
  ): Promise<any> {
    if (this.config.carrier !== 'ups') {
      const merchant = new MerchantService();
      const californiaAddresses = require('../../data/addresses/CA.json');
      await merchant.postCreateNewMerchant();

      const registerCarrierBody: any = await merchant.registerUpsAsCarrier(
        californiaAddresses.filter(
          (add: IAddress) => add.city === 'San Francisco'
        )[0]
      );
      registerCarrierBody.parameters.pickup_address_same_as_billing_address =
        true;
      delete registerCarrierBody.parameters.pickup_address_city;
      delete registerCarrierBody.parameters.pickup_address_state;
      delete registerCarrierBody.parameters.pickup_address_street1;
      delete registerCarrierBody.parameters.pickup_address_street2;
      delete registerCarrierBody.parameters.pickup_address_zip;
      return merchant.postMerchantRegistration(
        registerCarrierBody,
        this.config
      );
    } else {
      this.merchantId = pickRandomElementFromList(carrierRegisteredWithUPS)[0];
      return {
        status: 201,
        data: {
          carrier: 'ups',
          object_id: 'cbec0b237a4c4e229d8f78e1ad1ab74e',
          object_owner: '199_c5b9d843-edf2-455f-9115-f631ee309e26',
          account_id: 'shippo_ups_account',
          parameters: {},
          test: false,
          active: true,
          is_shippo_account: true,
          metadata: '',
          carrier_name: 'UPS',
          carrier_images: {
            200: 'https://dev-qa-static-shippodev-com.s3.amazonaws.com/providers/200/UPS.png',
            75: 'https://dev-qa-static-shippodev-com.s3.amazonaws.com/providers/75/UPS.png'
          }
        }
      };
    }
  }

  async attemptToRegisterACarrierWithMissingProp(
    property: string,
    isPositiveFlow?: boolean
  ): Promise<any> {
    const merchant = new MerchantService();
    const californiaAddresses = require('../../data/addresses/CA.json');
    await merchant.postCreateNewMerchant();

    const registerCarrierBody: any = await merchant.registerUpsAsCarrier(
      californiaAddresses.filter(
        (add: IAddress) => add.city === 'San Francisco'
      )[0]
    );
    registerCarrierBody.parameters.pickup_address_same_as_billing_address =
      false;
    delete registerCarrierBody.parameters[property];

    return merchant.postMerchantRegistration(registerCarrierBody, this.config);
  }
}
