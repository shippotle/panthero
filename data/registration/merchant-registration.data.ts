import { MerchantService } from '@services/merchant-services';
import { AxiosResponse } from 'axios';
import { IConfig, defaultConfig } from '@utils/common.util';

export class MerchantRegistrationData {
  merchant;
  merchantId: string = '';
  config: IConfig;
  constructor(config?: IConfig) {
    this.config = {
      ...defaultConfig,
      ...config
    };
    this.merchant = new MerchantService(this.config);
  }

  async merchantCreation(): Promise<string> {
    const merchantResponse = await this.merchant.postCreateNewMerchant();
    this.merchantId = merchantResponse.data.object_id;
    return this.merchantId;
  }

  async merchantRegistration(
    config: IConfig = this.config,
    merchantId: string
  ): Promise<AxiosResponse> {
    return this.merchant.postMerchantRegistration('', config, merchantId);
  }
}
