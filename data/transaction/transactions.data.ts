import { MerchantService } from '@services/merchant-services';
import { AxiosResponse } from 'axios';
import { httpRequest } from '@utils/helpers';
import { ShipmentData } from '@data/shipment.data';
import { TransactionServices } from '@services/transaction-services';
import { WebhooksServices } from '@services/webhooks-services';
import { IConfig, defaultConfig } from '@utils/common.util';

export class TransactionsData {
  private rate: any;
  private shipment;
  private merchant: any;
  private transaction: any;
  private transactionBody: any;
  public shipmentResponse: any;
  public transactionId: string = '';
  public merchantId: string = '';
  config: IConfig;
  constructor(config?: IConfig) {
    this.config = {
      ...defaultConfig,
      ...config
    };
    this.shipment = new ShipmentData(this.config);
    this.merchant = new MerchantService(this.config);
    this.transaction = new TransactionServices(this.config);
  }

  async getAllTransactions(merchantId: string) {
    return httpRequest(
      this.transaction.transactionUrl(merchantId),
      'GET',
      this.transaction.headers(merchantId)
    );
  }

  async getTransactionById(transactionId: string, merchantId: string) {
    const responseTransaction = await httpRequest(
      `${this.transaction.transactionUrl(merchantId)}${transactionId}`,
      'GET',
      this.transaction.headers(merchantId)
    );
    return responseTransaction;
  }

  async createATransaction(): Promise<AxiosResponse> {
    this.shipmentResponse = await this.shipment.createAShipment(this.config);

    this.rate = this.shipmentResponse.data.rates[0].object_id;

    this.merchantId = this.shipment.merchantId;

    this.transactionBody = this.merchant.createTransaction(this.rate);

    const transactionResponse = await this.transaction.postATransaction(
      this.transactionBody,
      this.merchantId
    );

    if (
      transactionResponse.status !== 201 ||
      transactionResponse.data === {} ||
      transactionResponse.data.status !== 'SUCCESS' ||
      transactionResponse.data.label_url === ''
    ) {
      if (
        transactionResponse.data.status !== 'SUCCESS' ||
        transactionResponse.data.label_url === ''
      ) {
        console.log('Shipment Response status: ', this.shipmentResponse.status);
        console.log('Shipment Response body: ', this.shipmentResponse.data);
        console.log('Transaction Request body: ', this.transactionBody);
        console.log(
          'Transaction Response status: ',
          transactionResponse.status
        );
        console.log('Transaction Response body: ', transactionResponse.data);
        throw new Error(
          'Create Transaction - Shipment API call did not return any rates or failed'
        );
      } else {
        console.log('Transaction Request body: ', this.transactionBody);
        console.log(
          'Transaction Response status: ',
          transactionResponse.status
        );
        console.log('Transaction Response body: ', transactionResponse.data);
        throw new Error(
          'Transaction API call did not return any rates or failed'
        );
      }
    }

    this.transactionId = transactionResponse.data.object_id;
    return transactionResponse;
  }

  async createInternationalTransaction(): Promise<AxiosResponse> {
    this.shipmentResponse = await this.shipment.createAnInternationalShipment();

    this.rate = this.shipmentResponse.data.rates[0].object_id;

    this.merchantId = this.shipment.merchantId;

    this.transactionBody = this.merchant.createTransaction(this.rate);

    const transactionResponse = await this.transaction.postATransaction(
      this.transactionBody,
      this.merchantId
    );

    if (
      transactionResponse.status !== 201 ||
      transactionResponse.data === {} ||
      transactionResponse.data.status !== 'SUCCESS' ||
      transactionResponse.data.label_url === ''
    ) {
      if (
        transactionResponse.data.status !== 'SUCCESS' ||
        transactionResponse.data.label_url === ''
      ) {
        console.log('Shipment Response status: ', this.shipmentResponse.status);
        console.log('Shipment Response body: ', this.shipmentResponse.data);
        console.log('Transaction Request body: ', this.transactionBody);
        console.log(
          'Transaction Response status: ',
          transactionResponse.status
        );
        console.log('Transaction Response body: ', transactionResponse.data);
        throw new Error(
          'Create Transaction - Shipment API call did not return any rates or failed'
        );
      } else {
        console.log('Transaction Request body: ', this.transactionBody);
        console.log(
          'Transaction Response status: ',
          transactionResponse.status
        );
        console.log('Transaction Response body: ', transactionResponse.data);
        throw new Error(
          'Transaction API call did not return any rates or failed'
        );
      }
    }

    this.transactionId = transactionResponse.data.object_id;
    return transactionResponse;
  }

  async createATransactionWithTheMissingProperty(property: string) {
    this.shipmentResponse =
      await this.shipment.createAShipmentWithAMissingProperty(property);

    this.rate = this.shipmentResponse.data.rates[0].object_id;

    this.merchantId = this.shipment.merchantId;

    this.transactionBody = this.merchant.createTransaction(this.rate);

    const transactionResponse = await this.transaction.postATransaction(
      this.transactionBody,
      this.merchantId
    );

    if (
      (transactionResponse.status !== 201 || transactionResponse.data === {}) &&
      this.config.isPositiveFlow
    ) {
      console.log('Transaction Request body: ', this.transactionBody);
      console.log('Transaction Response status: ', transactionResponse.status);
      console.log('Transaction Response body: ', transactionResponse.data);
      throw new Error(
        'Transaction API call did not return any rates or failed'
      );
    }
    return transactionResponse;
  }

  async attemptToCreateATransactionNoMerchantRate() {
    this.shipmentResponse = await this.shipment.createAShipment();

    this.merchantId = this.shipment.merchantId;
    return this.transaction.postATransaction(
      this.transactionBody,
      this.merchantId
    );
  }

  async trackTheStatusForOrder(): Promise<AxiosResponse> {
    const w = new WebhooksServices();
    const transactionResponse = await this.createATransaction();
    const trackingStatus = w.postWebhook(
      transactionResponse.data.tracking_number,
      this.merchantId
    );
    return trackingStatus;
  }
}
