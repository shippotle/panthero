export const ErrorSchema = {
  type: 'object',
  required: ['status', 'error'],
  properties: {
    status: {
      type: 'number'
    },
    error: {
      type: 'string'
    }
  }
};

export const merchantRegistrationSchema = {
  type: 'object',
  properties: {
    carrier: {
      type: 'string'
    },
    object_id: {
      type: 'string'
    },
    object_owner: {
      type: 'string'
    },
    account_id: {
      type: 'string'
    },
    parameters: {
      type: 'object'
    },
    test: {
      type: 'boolean'
    },
    active: {
      type: 'boolean'
    },
    is_shippo_account: {
      type: 'boolean'
    },
    metadata: {
      type: 'string'
    },
    carrier_name: {
      type: 'string'
    },
    carrier_images: {
      type: 'object',
      properties: {
        '75': {
          type: 'string'
        },
        '200': {
          type: 'string'
        }
      },
      required: ['75', '200']
    }
  },
  required: [
    'carrier',
    'object_id',
    'object_owner',
    'account_id',
    'parameters',
    'test',
    'active',
    'is_shippo_account',
    'metadata',
    'carrier_name',
    'carrier_images'
  ]
};

export const merchantsResponseSchema = {
  type: 'object',
  additionalProperties: false,
  required: [
    'email',
    'first_name',
    'last_name',
    'merchant_name',
    'object_id',
    'object_created',
    'object_updated'
  ],
  properties: {
    email: {
      type: 'string'
    },
    first_name: {
      type: 'string'
    },
    last_name: {
      type: 'string'
    },
    merchant_name: {
      type: 'string'
    },
    object_created: {
      type: 'string'
    },
    object_id: {
      type: 'string'
    },
    object_updated: {
      type: 'string'
    },
    platform_id: {
      type: 'string'
    },
    preferred_currency: {
      type: 'string'
    },
    use_preferred_currency: {
      type: 'boolean'
    }
  }
};

export const AllmerchantsResponseSchema = {
  type: 'object',
  additionalProperties: false,
  required: ['results'],
  properties: {
    next: {
      type: ['string', 'null']
    },
    previous: {
      type: ['string', 'null']
    },
    results: {
      type: 'array',
      items: merchantsResponseSchema
    }
  }
};

export const merchantsAddress = {
  type: 'object',
  required: ['city', 'country', 'name', 'state', 'street1', 'zip'],
  additionalProperties: false,
  properties: {
    object_created: {
      type: 'string'
    },
    object_updated: {
      type: 'string'
    },
    object_id: {
      type: 'string'
    },
    is_complete: {
      type: 'boolean'
    },
    validation_results: {
      type: 'object',
      additionalProperties: false,
      properties: {
        is_valid: {
          type: 'boolean'
        },
        messages: {
          type: 'array',
          items: {
            type: 'object',
            additionalProperties: false,
            properties: {
              code: {
                enum: [
                  'verification_error',
                  'unknown_street',
                  'component_mismatch_error',
                  'multiple_match',
                  'sub_premise_number_invalid',
                  'sub_premise_number_missing',
                  'premise_number_invalid',
                  'premise_number_missing',
                  'box_number_invalid',
                  'box_number_missing',
                  'pmb_number_missing',
                  'postal_code_change',
                  'administrative_area_change',
                  'locality_change',
                  'dependent_locality_change',
                  'street_name_change',
                  'street_type_change',
                  'street_directional_change',
                  'sub_premise_type_change',
                  'sub_premise_number_change',
                  'double_dependent_locality_change',
                  'subadministrative_area_change',
                  'subnational_area_change',
                  'po_box_change',
                  'premise_type_change',
                  'house_number_change',
                  'organization_change',
                  'extraneous_information',
                  'usps_door_inaccessible',
                  'administrative_area_partial',
                  'city_partial',
                  'street_partial',
                  'building_partial',
                  'subpremise_partial',
                  'administrative_area_full',
                  'city_full',
                  'thoroughfare_full',
                  'premises_full',
                  'subpremise_full',
                  'geocoded_street',
                  'geocoded_neighborhood',
                  'geocoded_community',
                  'geocoded_state',
                  'geocoded_rooftop',
                  'geocoded_interpolated_rooftop',
                  'invalid_postal_code',
                  'postal_code_not_found',
                  'empty_request',
                  'service_error',
                  'street_detail_missing',
                  'Invalid City/State/Zip',
                  'Default Match',
                  'Unknown Street',
                  'Address Not Found',
                  'Non-Deliverable ZIP4',
                  'Multiple Responses',
                  'Invalid Dual Address',
                  'Invalid State',
                  'Invalid City',
                  'Ambiguous Address'
                ]
              },
              source: {
                enum: ['Shippo Address Validator', 'UPS ']
              },
              text: {
                type: 'string'
              },
              type: {
                type: 'string'
              }
            }
          }
        }
      }
    },
    object_owner: {
      type: 'string'
    },
    name: {
      type: 'string'
    },
    company: {
      type: 'string'
    },
    street_no: {
      type: 'string'
    },
    street1: {
      type: 'string'
    },
    street2: {
      type: ['string', 'null']
    },
    street3: {
      type: ['string', 'null']
    },
    city: {
      type: 'string'
    },
    state: {
      type: 'string'
    },
    zip: {
      type: 'string'
    },
    country: {
      type: 'string'
    },
    longitude: {
      type: ['number', 'string', 'null']
    },
    latitude: {
      type: ['number', 'string', 'null']
    },
    phone: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    is_residential: {
      type: 'boolean'
    },
    metadata: {
      type: 'string'
    },
    test: {
      type: 'boolean'
    }
  }
};

export const AllmerchantsAddressResponseSchema = {
  type: 'object',
  additionalProperties: false,
  required: ['results'],
  properties: {
    next: {
      type: ['string', 'null']
    },
    previous: {
      type: ['string', 'null']
    },
    results: {
      type: 'array',
      items: merchantsAddress
    }
  }
};

export const merchantsParcel = {
  type: 'object',
  additionalProperties: false,
  required: ['mass_unit', 'weight'],
  properties: {
    object_state: {
      type: ['string', 'null']
    },
    object_created: {
      type: ['string', 'null']
    },
    object_updated: {
      type: ['string', 'null']
    },
    object_id: {
      type: ['string', 'null']
    },
    object_owner: {
      type: ['string', 'null']
    },
    template: {
      type: ['string', 'null'],
      enum: [
        'FedEx_Box_10kg',
        'FedEx_Box_25kg',
        'FedEx_Box_Extra_Large_1',
        'FedEx_Box_Extra_Large_2',
        'FedEx_Box_Large_1',
        'FedEx_Box_Large_2',
        'FedEx_Box_Medium_1',
        'FedEx_Box_Medium_2',
        'FedEx_Box_Small_1',
        'FedEx_Box_Small_2',
        'FedEx_Envelope',
        'FedEx_Padded_Pak',
        'FedEx_Pak_1',
        'FedEx_Pak_2',
        'FedEx_Tube',
        'FedEx_XL_Pak',
        'UPS_Box_10kg',
        'UPS_Box_25kg',
        'UPS_Express_Box',
        'UPS_Express_Box_Large',
        'UPS_Express_Box_Medium',
        'UPS_Express_Box_Small',
        'UPS_Express_Envelope',
        'UPS_Express_Hard_Pak',
        'UPS_Express_Legal_Envelope',
        'UPS_Express_Pak',
        'UPS_Express_Tube',
        'UPS_Laboratory_Pak',
        'UPS_MI_BPM',
        'UPS_MI_BPM_Flat',
        'UPS_MI_BPM_Parcel',
        'UPS_MI_First_Class',
        'UPS_MI_Flat',
        'UPS_MI_Irregular',
        'UPS_MI_Machinable',
        'UPS_MI_MEDIA_MAIL',
        'UPS_MI_Parcel_Post',
        'UPS_MI_Priority',
        'UPS_MI_Standard_Flat',
        'UPS_Pad_Pak',
        'UPS_Pallet',
        'USPS_FlatRateCardboardEnvelope',
        'USPS_FlatRateEnvelope',
        'USPS_FlatRateGiftCardEnvelope',
        'USPS_FlatRateLegalEnvelope',
        'USPS_FlatRatePaddedEnvelope',
        'USPS_FlatRateWindowEnvelope',
        'USPS_IrregularParcel',
        'USPS_LargeFlatRateBoardGameBox',
        'USPS_LargeFlatRateBox',
        'USPS_APOFlatRateBox',
        'USPS_LargeVideoFlatRateBox',
        'USPS_MediumFlatRateBox1',
        'USPS_MediumFlatRateBox2',
        'USPS_RegionalRateBoxA1',
        'USPS_RegionalRateBoxA2',
        'USPS_RegionalRateBoxB1',
        'USPS_RegionalRateBoxB2',
        'USPS_SmallFlatRateBox',
        'USPS_SmallFlatRateEnvelope',
        'USPS_SoftPack',
        'DHLeC_Irregular',
        'DHLeC_SM_Flats',
        'couriersplease_500g_satchel',
        'couriersplease_1kg_satchel',
        'couriersplease_3kg_satchel',
        'couriersplease_5kg_satchel',
        'Fastway_Australia_Satchel_A2',
        'Fastway_Australia_Satchel_A3',
        'Fastway_Australia_Satchel_A4',
        'Fastway_Australia_Satchel_A5'
      ]
    },
    length: {
      type: ['string', 'null']
    },
    width: {
      type: 'string'
    },
    height: {},
    distance_unit: {
      enum: ['cm', 'in', 'ft', 'm', 'mm', 'yd']
    },
    weight: {
      type: 'string'
    },
    mass_unit: {
      enum: ['g', 'kg', 'lb', 'oz']
    },
    value_amount: {
      type: ['string', 'null']
    },
    value_currency: {
      type: ['string', 'null']
    },
    metadata: {
      type: ['string', 'null']
    },
    extra: {
      type: 'object',
      additionalProperties: false,
      properties: {
        COD: {
          type: 'object',
          additionalProperties: false,
          properties: {
            amount: {
              type: ['string', 'null']
            },
            currency: {
              type: ['string', 'null']
            },
            payment_method: {
              enum: ['SECURED_FUNDS', 'CASH', 'ANY']
            }
          }
        },
        insurance: {
          type: 'object',
          additionalProperties: false,
          properties: {
            amount: {
              type: ['string', 'null']
            },
            content: {
              type: ['string', 'null']
            },
            currency: {
              type: ['string', 'null']
            },
            provider: {
              type: ['string', 'null']
            }
          }
        }
      }
    },
    test: {
      type: ['boolean', 'null']
    },
    line_items: {
      type: 'array',
      items: {
        type: ['string', 'null']
      }
    }
  }
};

export const merchantsParcels = {
  type: 'array',
  items: merchantsParcel
};

export const merchantsErrorSchema = {
  type: 'object',
  additionalProperties: false,
  patternProperties: {
    '(email|first_name|last_name|merchant_name|country|mass_unit|__all__|rate|preferred_currency|use_preferred_currency:)':
      {
        type: 'array',
        items: {
          type: 'string'
        }
      }
  }
};
