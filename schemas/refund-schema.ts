export const refundResponseSchema = {
  type: 'object',
  required: ['object_id', 'object_owner', 'status'],
  additionalProperties: false,
  properties: {
    object_state: {
      type: 'string'
    },
    status: {
      enum: [
        'WAITING',
        'QUEUED',
        'SUCCESS',
        'ERROR',
        'REFUNDED',
        'REFUNDPENDING',
        'REFUNDREJECTED'
      ]
    },
    object_created: {
      type: 'string'
    },
    object_updated: {
      type: 'string'
    },
    object_id: {
      type: 'string'
    },
    object_owner: {
      type: 'string'
    },
    test: {
      type: 'boolean'
    },
    rate: {
      type: 'string'
    },
    tracking_number: {
      type: 'string'
    },
    tracking_status: {
      type: 'string'
    },
    eta: {
      type: ['string', 'null']
    },
    tracking_url_provider: {
      type: 'string'
    },
    label_url: {
      type: 'string'
    },
    commercial_invoice_url: {
      type: ['string', 'null']
    },
    messages: {
      type: 'array',
      items: {
        type: 'object'
      }
    },
    order: {
      type: ['string', 'null']
    },
    metadata: {
      type: 'string'
    },
    parcel: {
      type: ['string', 'null']
    },
    billing: {
      type: 'object',
      properties: {
        payments: {
          type: 'array',
          items: {
            type: 'object'
          }
        }
      }
    },
    qr_code_url: {
      type: ['string', 'null']
    }
  }
};

export const refundsErrorSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    transaction: {
      type: 'array',
      items: {
        type: 'string'
      }
    }
  }
};
