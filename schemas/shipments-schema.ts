const addressSchema = {
  type: ['object', 'null'],
  properties: {
    object_id: {
      type: 'string'
    },
    is_complete: {
      type: 'boolean'
    },
    name: {
      type: 'string'
    },
    company: {
      type: 'string'
    },
    street_no: {
      type: 'string'
    },
    street1: {
      type: 'string'
    },
    validation_results: {
      type: 'object'
    },
    street2: {
      type: 'string'
    },
    street3: {
      type: 'string'
    },
    city: {
      type: 'string'
    },
    state: {
      type: 'string'
    },
    zip: {
      type: 'string'
    },
    country: {
      type: 'string'
    },
    phone: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    is_residential: {
      type: 'null'
    },
    test: {
      type: 'boolean'
    }
  },
  required: [
    'object_id',
    'is_complete',
    'name',
    'company',
    'street_no',
    'street1',
    'validation_results',
    'street2',
    'street3',
    'city',
    'state',
    'zip',
    'country',
    'phone',
    'email',
    'is_residential',
    'test'
  ]
};

export const shipmentsResponseSchema = {
  type: 'object',
  properties: {
    carrier_accounts: {
      type: 'array',
      items: {}
    },
    object_created: {
      type: 'string'
    },
    object_updated: {
      type: 'string'
    },
    object_id: {
      type: 'string'
    },
    object_owner: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    address_from: addressSchema,
    address_to: addressSchema,
    parcels: {
      type: 'array',
      items: [
        {
          type: 'object',
          properties: {
            object_state: {
              type: ['string', 'null']
            },
            object_created: {
              type: ['string', 'null']
            },
            object_updated: {
              type: ['string', 'null']
            },
            object_id: {
              type: ['string', 'null']
            },
            object_owner: {
              type: ['string', 'null']
            },
            template: {
              type: ['string', 'null']
            },
            extra: {
              type: ['object', 'null'],
              properties: {
                COD: {
                  type: 'object',
                  additionalProperties: false,
                  properties: {
                    amount: {
                      type: ['string', 'null']
                    },
                    currency: {
                      type: ['string', 'null']
                    },
                    payment_method: {
                      enum: ['SECURED_FUNDS', 'CASH', 'ANY']
                    }
                  }
                },
                insurance: {
                  type: 'object',
                  additionalProperties: false,
                  properties: {
                    amount: {
                      type: ['string', 'null']
                    },
                    content: {
                      type: ['string', 'null']
                    },
                    currency: {
                      type: ['string', 'null']
                    },
                    provider: {
                      type: ['string', 'null']
                    }
                  }
                }
              }
            },
            length: {
              type: ['string', 'null']
            },
            width: {
              type: ['string', 'null']
            },
            height: {
              type: ['string', 'null']
            },
            distance_unit: {
              type: ['string', 'null'],
              enum: ['cm', 'in', 'ft', 'm', 'mm', 'yd']
            },
            weight: {
              type: ['string', 'null']
            },
            mass_unit: {
              type: ['string', 'null'],
              enum: ['g', 'kg', 'lb', 'oz']
            },
            value_amount: {
              type: ['string', 'null']
            },
            value_currency: {
              type: ['string', 'null']
            },
            metadata: {
              type: ['string', 'null']
            },
            line_items: {
              type: 'array',
              items: {
                type: ['string', 'null']
              }
            },
            test: {
              type: ['boolean', 'null']
            }
          },
          additionalProperties: false,
          required: ['mass_unit', 'weight']
        }
      ]
    },
    shipment_date: {
      type: ['string', 'null']
    },
    address_return: addressSchema,
    alternate_address_to: addressSchema,
    customs_declaration: {
      type: ['string', 'null']
    },
    extra: {
      type: 'object'
    },
    rates: {
      type: 'array',
      items: [
        {
          type: 'object',
          properties: {
            object_created: {
              type: 'string'
            },
            object_id: {
              type: 'string'
            },
            object_owner: {
              type: 'string'
            },
            shipment: {
              type: 'string'
            },
            attributes: {
              type: 'array',
              items: {}
            },
            amount: {
              type: 'string'
            },
            currency: {
              type: 'string'
            },
            amount_local: {
              type: 'string'
            },
            currency_local: {
              type: 'string'
            },
            provider: {
              type: 'string'
            },
            provider_image_75: {
              type: 'string'
            },
            provider_image_200: {
              type: 'string'
            },
            servicelevel: {
              type: 'object',
              properties: {
                name: {
                  type: 'string'
                },
                token: {
                  type: 'string'
                },
                terms: {
                  type: 'string'
                },
                extended_token: {
                  type: 'string'
                },
                parent_servicelevel: {
                  type: 'null'
                }
              },
              required: [
                'name',
                'token',
                'terms',
                'extended_token',
                'parent_servicelevel'
              ]
            },
            estimated_days: {
              type: 'integer'
            },
            arrives_by: {
              type: 'null'
            },
            duration_terms: {
              type: 'string'
            },
            messages: {
              type: 'array',
              items: {}
            },
            carrier_account: {
              type: 'string'
            },
            test: {
              type: 'boolean'
            },
            zone: {
              type: ['string', 'null']
            }
          },
          required: [
            'object_created',
            'object_id',
            'object_owner',
            'shipment',
            'attributes',
            'amount',
            'currency',
            'amount_local',
            'currency_local',
            'provider',
            'provider_image_75',
            'provider_image_200',
            'servicelevel',
            'estimated_days',
            'arrives_by',
            'duration_terms',
            'messages',
            'carrier_account',
            'test',
            'zone'
          ]
        }
      ]
    },
    messages: {
      type: 'array',
      items: [
        {
          type: 'object',
          properties: {
            source: {
              type: 'string'
            },
            code: {
              type: 'string'
            },
            text: {
              type: 'string'
            }
          },
          required: ['source', 'code', 'text']
        }
      ]
    },
    metadata: {
      type: 'string'
    },
    test: {
      type: 'boolean'
    },
    order: {
      type: ['string', 'null']
    }
  },
  required: [
    'carrier_accounts',
    'object_created',
    'object_updated',
    'object_id',
    'object_owner',
    'status',
    'address_from',
    'address_to',
    'parcels',
    'shipment_date',
    'address_return',
    'alternate_address_to',
    'customs_declaration',
    'extra',
    'rates',
    'messages',
    'metadata',
    'test',
    'order'
  ]
};
