export const transactionResponseSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    commercial_invoice_url: {
      type: ['string', 'null']
    },
    order: {
      type: ['string', 'null']
    },
    parcel: {
      type: ['string', 'null']
    },
    billing: {
      type: 'object',
      properties: {
        payments: {
          type: 'array',
          items: {
            type: 'object'
          }
        }
      }
    },
    eta: {
      type: ['string', 'null']
    },
    label_url: {
      type: 'string'
    },
    messages: {
      type: 'array',
      items: {
        type: 'object'
      }
    },
    metadata: {
      type: 'string'
    },
    object_created: {
      type: 'string'
    },
    object_id: {
      type: 'string'
    },
    object_owner: {
      type: 'string'
    },
    object_state: {
      enum: ['VALID', 'INVALID']
    },
    object_updated: {
      type: 'string'
    },
    qr_code_url: {
      type: ['string', 'null']
    },
    rate: {
      type: 'string'
    },
    status: {
      enum: [
        'WAITING',
        'QUEUED',
        'SUCCESS',
        'ERROR',
        'REFUNDED',
        'REFUNDPENDING',
        'REFUNDREJECTED'
      ]
    },
    test: {
      type: 'boolean'
    },
    tracking_number: {
      type: 'string'
    },
    tracking_status: {
      type: 'string'
    },
    tracking_url_provider: {
      type: 'string'
    }
  }
};

export const getTransactionsResponseSchema = {
  type: 'object',
  additionalProperties: false,
  required: ['results'],
  properties: {
    next: {
      type: ['string', 'null']
    },
    previous: {
      type: ['string', 'null']
    },
    results: {
      type: 'array',
      items: transactionResponseSchema
    }
  }
};
