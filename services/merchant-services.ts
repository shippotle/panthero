import * as faker from 'faker';
import { pickRandomElementFromList } from '@utils/helpers';
import { AxiosResponse, Method } from 'axios';
import { httpRequest, apiCallRetrySchema } from '@utils/helpers';
import { envs, headers } from '@utils/common.util';
import { IConfig, defaultConfig } from '@utils/common.util';
export class MerchantService {
  private readonly email: string;
  private readonly first_name: string;
  private readonly last_name: string;
  private readonly merchant_name: string;
  private readonly addressFrom: IAddress;
  private readonly merchantBody: any;
  private readonly carrierRegistrationBody: any;
  public headers;
  merchantId: string = '';
  merchantResponse = {};
  merchantUrl = (merchantId: string = '') => {
    if (process.env.isPlatformUrl === 'false') {
      if (merchantId) return `${envs?.url}/shippo-accounts/${merchantId}`;
      else return `${envs?.url}/shippo-accounts/`;
    } else return `${envs?.url}/merchants/${merchantId}`;
  };
  addressUrl = (merchantId?: string): string => {
    if (process.env.isPlatformUrl === 'false') {
      return `${envs?.url}/addresses/`;
    } else return `${envs?.url}/merchants/${merchantId}/addresses/`;
  };
  registrationStatusUrl = (merchantId: string): string => {
    if (process.env.isPlatformUrl === 'false') {
      return `${envs?.url}/carrier-accounts/reg-status/`;
    } else
      return `${envs?.url}/merchants/${merchantId}/carrier_accounts/reg-status/`;
  };
  carrierRegistrationsUrl = (merchantId: string): string => {
    if (process.env.isPlatformUrl === 'false') {
      return `${envs?.url}/carrier_accounts/register/new`;
    } else
      return `${envs?.url}/merchants/${merchantId}/carrier_accounts/register/new`;
  };
  config: IConfig;
  constructor(config?: IConfig) {
    this.config = {
      ...defaultConfig,
      ...config
    };
    const addressFrom = require(`../data/addresses/${this.config.stateFrom}.json`);
    this.addressFrom = pickRandomElementFromList(addressFrom)[0];
    this.email = `platform+${faker.datatype
      .uuid()
      .replace(/-/g, '')}@goshippo.com`;
    this.first_name = faker.name.firstName();
    this.last_name = faker.name.lastName();
    this.merchant_name = `My Beautiful ${this.first_name}-${this.last_name} Shop`;
    this.merchantBody = this.createNewMerchant(config);
    this.headers = (merchantId: string = '', conf: IConfig = this.config) =>
      headers(merchantId, conf);
  }

  createNewMerchant(config?: IConfig): IMerchant {
    const body: any = {
      email: this.email,
      first_name: this.first_name,
      last_name: this.last_name,
      merchant_name: this.merchant_name
    };
    if (config?.preferred_currency) {
      body.preferred_currency = config?.preferred_currency;
      body.use_preferred_currency = config?.use_preferred_currency;
    }
    return body;
  }

  async postCreateNewMerchant(body?: any): Promise<AxiosResponse> {
    const merchantResponse = await httpRequest(
      this.merchantUrl(),
      'POST',
      this.headers(),
      body ?? this.merchantBody
    );
    this.merchantId = merchantResponse.data.object_id;
    this.merchantResponse = merchantResponse.data;
    return merchantResponse;
  }

  async putAlterMerchant(
    merchantId: string,
    body?: any
  ): Promise<AxiosResponse> {
    const merchantResponse = await httpRequest(
      this.merchantUrl(merchantId),
      'PUT',
      this.headers(),
      body ?? this.merchantBody
    );
    this.merchantId = merchantResponse.data.object_id;
    this.merchantResponse = merchantResponse.data;
    return merchantResponse;
  }

  getCarrierRegistrationStatus = async (
    merchantId: string
  ): Promise<AxiosResponse> => {
    let statusResponse = await httpRequest(
      this.registrationStatusUrl(merchantId),
      'GET',
      this.headers(merchantId)
    );

    const call = {
      response: statusResponse,
      url: this.registrationStatusUrl(merchantId),
      method: 'GET' as Method,
      headers: this.headers(merchantId),
      numberOfTimes: 3
    };
    statusResponse = await apiCallRetrySchema(call, this.config);
    if (statusResponse.status !== 200) {
      console.log('Get Carrier Reg. Response status: ', statusResponse.status);
      console.log('Carrier Reg. Response body: ', statusResponse.data);
      throw new Error('Get Carrier Reg. Carrier Reg. API call failed');
    }

    return statusResponse;
  };

  getCarrierRegistration = async (
    merchantId: string
  ): Promise<AxiosResponse> => {
    const statusResponse = await httpRequest(
      `${this.merchantUrl(merchantId)}`,
      'GET',
      this.headers(merchantId)
    );
    return statusResponse;
  };

  postToAddresses = async (
    merchantId: string,
    addressObject: any
  ): Promise<AxiosResponse> => {
    const body = {
      city: addressObject.city,
      company: 'My Company',
      country: 'US',
      email: 'shippotle@goshippo.com',
      is_residential: true,
      metadata: '',
      name: 'Shwan Ippotle',
      phone: '+1 555 341 9393',
      state: addressObject.state,
      street1: addressObject.address1,
      street2: addressObject.address2,
      validate: true,
      zip: addressObject.postalCode
    };
    await new Promise((resolve) => setTimeout(resolve, 1500));

    return httpRequest(this.addressUrl(merchantId), 'POST', this.headers, body);
  };

  async postMerchantRegistration(
    body?: any,
    config: IConfig = this.config,
    merchantId?: string
  ): Promise<AxiosResponse> {
    let merchantResponse = await httpRequest(
      this.carrierRegistrationsUrl(merchantId ?? this.merchantId),
      'POST',
      this.headers(merchantId),
      body
        ? body
        : this.returnCarrierBody(config ? config.carrier : this.config.carrier)
    );

    const call = {
      response: merchantResponse,
      url: this.carrierRegistrationsUrl(this.merchantId),
      method: 'POST' as Method,
      headers: this.headers,
      body: body ?? this.carrierRegistrationBody,
      message: 'An unexpected error occurred while registering UPS account',
      numberOfTimes: 3
    };

    if (
      merchantResponse.status !== 201 &&
      (config.isPositiveFlow ?? this.config.isPositiveFlow)
    ) {
      merchantResponse = await apiCallRetrySchema(call, config ?? this.config);
    }

    if (
      merchantResponse.status !== 201 &&
      (config.isPositiveFlow ?? this.config.isPositiveFlow)
    ) {
      console.log(
        'Carrier Reg. Request body: ',
        body ?? this.carrierRegistrationBody
      );
      console.log('Carrier Reg. Response status: ', merchantResponse.status);
      console.log('Carrier Reg. Response body: ', merchantResponse.data);
      throw new Error('Carrier Reg. API call failed');
    }
    return merchantResponse;
  }

  registerUspsCarrier() {
    return {
      carrier: 'usps',
      parameters: {}
    };
  }

  registerHermesUkCarrier() {
    return {
      carrier: 'hermes_uk',
      parameters: {}
    };
  }

  registerDpdUkCarrier() {
    return {
      carrier: 'dpd_uk',
      parameters: {}
    };
  }

  registerHermesDECarrier() {
    return {
      carrier: 'hermes_de',
      parameters: {}
    };
  }

  registerCanadaPostCarrier() {
    return {
      carrier: 'canada_post',
      parameters: {
        canada_post_terms: true,
        company: this.merchant_name,
        email: this.email,
        full_name: this.first_name,
        phone: faker.phone.phoneNumber()
      },
      test: true,
      active: true,
      is_shippo_account: false
    };
  }

  registerUpsAsCarrier(
    address: IAddress = this.addressFrom
  ): IRegisterUpsAsCarrier {
    return {
      carrier: 'ups',
      parameters: {
        billing_address_city: address.city,
        billing_address_country_iso2: 'US',
        billing_address_state: address.state,
        billing_address_street1: address.address1,
        billing_address_street2: address.address2,
        billing_address_zip: address.postalCode,
        company: this.merchant_name,
        email: this.email,
        full_name: this.first_name,
        phone: faker.phone.phoneNumber(),
        pickup_address_city: address.city,
        pickup_address_country_iso2: address.country ?? 'US',
        pickup_address_same_as_billing_address: true,
        pickup_address_state: address.state,
        pickup_address_street1: address.address1,
        pickup_address_street2: address.address2,
        pickup_address_zip: address.postalCode,
        ups_agreements: true
      }
    };
  }

  returnCarrierBody(carrier?: string) {
    switch (carrier) {
      case 'usps': {
        return this.registerUspsCarrier();
      }
      case 'ups': {
        return this.registerUpsAsCarrier();
      }
      case 'hermes_uk': {
        return this.registerHermesUkCarrier();
      }
      case 'hermes_de': {
        return this.registerHermesDECarrier();
      }
      case 'dpd_uk': {
        return this.registerDpdUkCarrier();
      }
      case 'canada_post': {
        return this.registerCanadaPostCarrier();
      }
      default: {
        return this.registerUspsCarrier();
      }
    }
  }

  createTransaction(rate: string): any {
    return {
      rate,
      label_file_type: 'PDF',
      async: false
    };
  }
}

export interface IRegisterUpsAsCarrier {
  carrier: string;
  parameters: {
    billing_address_city?: string;
    billing_address_country_iso2?: string;
    billing_address_state?: string;
    billing_address_street1?: string;
    billing_address_street2?: string;
    billing_address_zip?: string;
    company?: string;
    email?: string;
    full_name: string;
    phone?: string;
    pickup_address_city?: string;
    pickup_address_country_iso2?: string;
    pickup_address_same_as_billing_address?: boolean;
    pickup_address_state?: string;
    pickup_address_street1?: string;
    pickup_address_street2?: string;
    pickup_address_zip?: string;
    ups_agreements?: boolean;
  };
}

export interface IAddress {
  address1: string;
  address2: string;
  city: string;
  state: string;
  postalCode: string;
  country: string;
  coordinates: { lat: number; lng: number };
}

export interface IMerchant {
  email: string;
  first_name: string;
  last_name: string;
  merchant_name: string;
  preferred_currency?: string;
  use_preferred_currency?: boolean;
}
