import { AxiosResponse } from 'axios';
import { httpRequest } from '@utils/helpers';
import { defaultConfig, envs, headers, IConfig } from '@utils/common.util';

export class WebhooksServices {
  config: IConfig;
  public headers;
  webHookUrl = (merchantId: string = '') => {
    if (process.env.isPlatformUrl === 'false') {
      return `${envs?.url}//tracks/`;
    } else return `${envs?.url}/merchants/${merchantId}/tracks/`;
  };
  constructor(config?: IConfig) {
    this.config = {
      ...defaultConfig,
      ...config
    };
    this.headers = (merchantId: string = '', conf: IConfig = this.config) =>
      headers(merchantId, conf);
  }
  async postWebhook(
    tracking_number: string,
    merchantId: string,
    carrier?: string
  ): Promise<AxiosResponse> {
    await new Promise((resolve) => setTimeout(resolve, 3000));
    const body = {
      carrier: carrier ?? 'shippo',
      metadata: 'Some order #000123',
      tracking_number:
        process.env.TOKEN === 'TEST' ? 'SHIPPO_DELIVERED' : tracking_number
    };

    const webHookResponse = await httpRequest(
      this.webHookUrl(merchantId),
      'POST',
      this.headers(merchantId),
      body
    );
    return webHookResponse;
  }

  async getStatusByTrackingNumber(
    tracking_number: string,
    merchantId: string,
    carrier?: string
  ): Promise<AxiosResponse> {
    await new Promise((resolve) => setTimeout(resolve, 3000));

    const webHookResponse = await httpRequest(
      `${this.webHookUrl(merchantId)}${carrier ?? 'ups'}/${tracking_number}`,
      'GET',
      this.headers(merchantId)
    );
    return webHookResponse;
  }
}
