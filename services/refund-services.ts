import { AxiosResponse } from 'axios';
import { httpRequest } from '@utils/helpers';
import { envs, headers, IConfig, defaultConfig } from '@utils/common.util';

export class RefundServices {
  public headers;
  config: IConfig;
  refundUrl = (merchantId: string = '') => {
    if (process.env.isPlatformUrl === 'false') {
      return `${envs?.url}/refunds/`;
    } else return `${envs?.url}/merchants/${merchantId}/refunds/`;
  };

  constructor(config?: IConfig) {
    this.config = {
      ...defaultConfig,
      ...config
    };
    this.headers = (merchantId: string = '', conf: IConfig = this.config) =>
      headers(merchantId, conf);
  }

  async postRefund(
    transactionId: string,
    merchantId: string,
    remove: boolean = false
  ): Promise<AxiosResponse> {
    const body = {
      transaction: transactionId,
      async: false
    };
    let newBody;
    if (remove) {
      const { transaction, ...rest } = body;
      newBody = rest;
    }
    const refundResponse = await httpRequest(
      this.refundUrl(merchantId),
      'POST',
      this.headers(merchantId),
      remove ? newBody : body
    );
    return refundResponse;
  }
}
