import { AxiosResponse, Method } from 'axios';
import { httpRequest, apiCallRetrySchema } from '@utils/helpers';
import { envs, headers } from '@utils/common.util';
import * as faker from 'faker';
import { pickRandomElementFromList } from '@utils/helpers';
import { IConfig, defaultConfig } from '@utils/common.util';

export class ShipmentService {
  private email: string;
  private readonly first_name: string;
  private readonly last_name: string;
  private merchant_name: string;
  private addressTo: IAddress;
  private addressFrom: IAddress;
  public headers;
  config: IConfig;
  shipmentsUrl = (merchantId: string = '') => {
    if (process.env.isPlatformUrl === 'false') {
      return `${envs?.url}/shipments/`;
    } else return `${envs?.url}/merchants/${merchantId}/shipments/`;
  };
  customDeclarationUrl = (merchantId: string = '') => {
    if (process.env.isPlatformUrl === 'false') {
      return `${envs?.url}/customs/declarations/`;
    } else return `${envs?.url}/merchants/${merchantId}/customs/declarations/`;
  };
  constructor(config?: IConfig) {
    this.config = {
      ...defaultConfig,
      ...config
    };
    const addressFrom = require(`../data/addresses/${
      config ? config.stateFrom : this.config.stateFrom
    }.json`);

    const addressTo = require(`../data/addresses/${
      config ? config.stateTo : this.config.stateTo
    }.json`);
    this.addressFrom = pickRandomElementFromList(addressFrom)[0];
    if (this.config.sameCity) {
      this.addressTo = addressTo.filter(
        (a: IAddress) =>
          a.city === this.addressFrom.city &&
          a.address1 !== this.addressFrom.address1
      )[0];
    } else
      this.addressTo = this.addressTo = addressTo.filter(
        (a: IAddress) => a.city !== this.addressFrom.city
      )[0];
    this.email = `platform+${faker.datatype
      .uuid()
      .replace(/-/g, '')}@goshippo.com`;
    this.first_name = faker.name.firstName();
    this.last_name = faker.name.lastName();
    this.merchant_name = `My ${this.first_name}-${this.last_name} Shop`;
    this.headers = (merchantId: string = '', conf: IConfig = this.config) =>
      headers(merchantId, conf);
  }

  createShipment(config?: IConfig): any {
    const defaultParcel = [
      {
        length: '1',
        width: '15',
        height: '2',
        distance_unit: 'in',
        weight: '3',
        mass_unit: 'lb'
      }
    ];

    return {
      address_to: {
        name: faker.name.firstName(),
        street1: this.addressTo.address1,
        street2: '',
        city: this.addressTo.city,
        state: this.addressTo.state,
        zip: this.addressTo.postalCode,
        country: this.addressTo.country ?? 'US',
        phone: faker.phone.phoneNumber(),
        email: `platform+${faker.datatype
          .uuid()
          .replace(/-/g, '')}@goshippo.com`
      },
      address_from: {
        name: faker.name.firstName(),
        street1: this.addressFrom.address1,
        street2: '',
        city: this.addressFrom.city,
        state: this.addressFrom.state,
        zip: this.addressFrom.postalCode,
        country: this.addressFrom.country ?? 'US',
        phone: faker.phone.phoneNumber(),
        email: `platform+${faker.datatype
          .uuid()
          .replace(/-/g, '')}@goshippo.com`
      },
      parcels: config?.parcels ?? defaultParcel,
      async: false
    };
  }

  async customDeclaration(
    merchantId: string,
    config?: IConfig
  ): Promise<AxiosResponse> {
    const customDeclBody = {
      aes_itn: 'string',
      b13a_filing_optio: 'string',
      b13a_numbe: 'string',
      certify: true,
      certify_signer: 'Shawn Ippotle',
      commercial_invoice: true,
      contents_explanation: 'T-Shirt purchase',
      contents_type: 'MERCHANDISE',
      disclaimer: 'string',
      eel_pfc: 'NOEEI_30_37_a',
      exporter_reference: 'string',
      importer_reference: 'string',
      incoterm: 'FCA',
      invoice: '#123123',
      items: [
        {
          description: 'T-Shirt',
          mass_unit: config?.parcels![0].mass_unit ?? 'g',
          net_weight: config?.parcels![0].weight ?? '5',
          origin_country: this.addressFrom.country ?? 'US',
          quantity: 1,
          tariff_number: '',
          value_amount: '120',
          value_currency: 'USD'
        }
      ],
      license: 'string',
      metadata: 'Order ID #123123',
      non_delivery_option: 'ABANDON',
      notes: 'string',
      test: true
    };
    return httpRequest(
      this.customDeclarationUrl(merchantId),
      'POST',
      this.headers(merchantId),
      customDeclBody
    );
  }

  async postShipment(
    shipmentBody: any,
    merchantId: string,
    isWithoutCarrierRegistration = false
  ): Promise<AxiosResponse> {
    let shipmentResponse: AxiosResponse;
    shipmentResponse = await httpRequest(
      this.shipmentsUrl(merchantId),
      'POST',
      this.headers(merchantId),
      shipmentBody
    );

    const call = {
      response: shipmentResponse,
      url: this.shipmentsUrl(merchantId),
      method: 'POST' as Method,
      headers: this.headers(merchantId),
      body: shipmentBody,
      message: 'UPS API did not respond. Please try again in a few minutes.',
      numberOfTimes: 5
    };
    if (
      shipmentResponse.data.error ||
      (this.config.isPositiveFlow &&
        shipmentResponse.data.rates.length === 0 &&
        !isWithoutCarrierRegistration)
    ) {
      shipmentResponse = await apiCallRetrySchema(call);
    }

    // Shipment APIs do not appear very responsive and sometimes rates are not returned , hence why the next line - make the problem visible
    if (
      (shipmentResponse.status !== 201 ||
        shipmentResponse.data.rates.length === 0) &&
      this.config.isPositiveFlow &&
      !isWithoutCarrierRegistration
    ) {
      console.log('Shipment Request body: ', shipmentBody);
      console.log('Shipment Response status: ', shipmentResponse.status);
      console.log('Shipment Response body: ', shipmentResponse.data);
      throw new Error('Shipment API call did not return any rates or failed');
    }
    return shipmentResponse;
  }

  async getShipmentRates(
    merchantId: string,
    shipmentId: string,
    currencyCode: string
  ): Promise<AxiosResponse> {
    let shipmentResponse: AxiosResponse;
    let i = 0;
    shipmentResponse = await httpRequest(
      `${this.shipmentsUrl(merchantId)}${shipmentId}/rates/${currencyCode}`,
      'GET',
      this.headers(merchantId)
    );

    if (shipmentResponse.data.results.length === 0) {
      do {
        await new Promise((resolve) => setTimeout(resolve, 5000));
        console.log(
          `Shipment API (rates) attempt #${
            i + 1
          } failed to respond! Trying again...`
        );

        shipmentResponse = await httpRequest(
          `${this.shipmentsUrl(merchantId)}${shipmentId}/rates/${currencyCode}`,
          'GET',
          this.headers(merchantId)
        );
        i++;
      } while (shipmentResponse.data.results.length === 0 && i < 5);
    }
    return shipmentResponse;
  }
}
export interface IAddress {
  address1: string;
  address2: string;
  country?: string;
  city: string;
  state: string;
  postalCode: string;
  coordinates: { lat: number; lng: number };
}
