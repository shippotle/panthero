import { defaultConfig, envs, headers, IConfig } from '@utils/common.util';
import { httpRequest } from '@utils/helpers';
import { AxiosResponse } from 'axios';

export class PickupServices {
  config: IConfig;
  public headers;
  pickUpUrl = (merchantId: string = '') => {
    if (process.env.isPlatformUrl === 'false') {
      return `${envs?.url}/pickups/`;
    } else return `${envs?.url}/merchants/${merchantId}/pickups/`;
  };
  constructor(config?: IConfig) {
    this.config = {
      ...defaultConfig,
      ...config
    };
    this.headers = (merchantId: string = '', conf: IConfig = this.config) =>
      headers(merchantId, conf);
  }
  async postCreatePickUp(
    merchantId: string,
    body: IPickUp
  ): Promise<AxiosResponse> {
    const pickupResponse = await httpRequest(
      this.pickUpUrl(merchantId),
      'POST',
      this.headers(merchantId),
      body
    );
    return pickupResponse;
  }

  createPickUp(transactionId: string): IPickUp {
    return {
      carrier_account: 'adcfdddf8ec64b84ad22772bce3ea37a',
      is_test: true,
      location: {
        address: {
          city: 'Flushing',
          country: 'US',
          email: 'pp@gmail.com',
          name: 'Peter Parker',
          phone: '909-929-9999',
          state: 'NY',
          street1: '20 Ingram St',
          zip: '11375'
        },
        building_location_type: building_location_type.Back,
        building_type: building_type.apartment,
        instructions: 'Behind screen door'
      },
      metadata: '',
      requested_end_time: '2022-03-06T00:54:11.833Z',
      requested_start_time: '2022-03-06T00:54:11.833Z',
      transactions: `[${transactionId}]`
    };
  }
}

enum building_location_type {
  Office = 'office',
  Other = 'other',
  Reception = 'reception',
  Back = 'back',
  Door = 'door',
  ring_bell = 'Ring Bell',
  security_eck = 'Security Deck',
  shipping_dock = 'Shipping Dock',
  front_door = 'Front Door',
  knock_door = 'Knock on Door',
  in_at_mailbox = 'In/At Mailbox',
  mail_room = 'Mail Room',
  side_door = 'Side Door'
}

enum building_type {
  apartment,
  building,
  department,
  floor,
  room,
  suite
}
export interface IPickUp {
  carrier_account: string;
  is_test: boolean;
  location: {
    address: {
      city: string;
      country: string;
      email: string;
      name: string;
      phone: string;
      state: string;
      street1: string;
      zip: string;
    };
    building_location_type: building_location_type;
    building_type: building_type;
    instructions: string;
  };
  metadata: string;
  requested_end_time: string;
  requested_start_time: string;
  transactions: string;
}
