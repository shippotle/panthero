import { AxiosResponse, Method } from 'axios';
import { httpRequest, apiCallRetrySchema } from '@utils/helpers';
import { defaultConfig, envs, headers, IConfig } from '@utils/common.util';

export class TransactionServices {
  config: IConfig;
  public headers;
  transactionUrl = (merchantId: string = '') => {
    if (process.env.isPlatformUrl === 'false') {
      return `${envs?.url}/transactions/`;
    } else return `${envs?.url}/merchants/${merchantId}/transactions/`;
  };
  constructor(config?: IConfig) {
    this.config = {
      ...defaultConfig,
      ...config
    };
    this.headers = (merchantId: string = '', conf: IConfig = this.config) =>
      headers(merchantId, conf);
  }
  async postATransaction(
    body: any,
    merchantId: string
  ): Promise<AxiosResponse> {
    let res = await httpRequest(
      this.transactionUrl(merchantId),
      'POST',
      this.headers(merchantId),
      body
    );

    const call = {
      response: res,
      url: this.transactionUrl(merchantId),
      method: 'POST' as Method,
      headers: this.headers(merchantId),
      body,
      message: 'UPS API did not respond. Please try again in a few minutes.',
      numberOfTimes: 3
    };

    if (
      (res.status !== 201 && this.config.isPositiveFlow) ||
      (res.data.status === 'ERROR' && this.config.isPositiveFlow)
    ) {
      res = await apiCallRetrySchema(call, this.config);
    }

    if (
      res.status !== 201 &&
      this.config.isPositiveFlow &&
      res.data.status === 'ERROR'
    ) {
      console.log('Transaction Response status: ', res.status);
      console.log('Transaction. Response body: ', res.data);
      throw new Error('Transaction API call failed');
    }
    return res;
  }

  async getAllTransactions(merchantId: string) {
    let responseTransactions = await httpRequest(
      this.transactionUrl(merchantId),
      'GET',
      this.headers(merchantId)
    );

    const call = {
      response: responseTransactions,
      url: this.transactionUrl(merchantId),
      method: 'GET' as Method,
      headers: this.headers(merchantId),
      numberOfTimes: 3
    };
    if (responseTransactions.status !== 200) {
      responseTransactions = await apiCallRetrySchema(call, this.config);
    }

    if (responseTransactions.status !== 200) {
      console.log(
        'Get all Transactions Response status: ',
        responseTransactions.status
      );
      throw new Error('Get all Transactions API call failed');
    }
    return responseTransactions;
  }

  async getTransactionById(transactionId: string, merchantId: string) {
    let responseTransaction = await httpRequest(
      `${this.transactionUrl(merchantId)}${transactionId}`,
      'GET',
      this.headers(merchantId)
    );

    const call = {
      response: responseTransaction,
      url: `${this.transactionUrl(merchantId)}${transactionId}`,
      method: 'GET' as Method,
      headers: this.headers(merchantId),
      numberOfTimes: 3
    };

    if (responseTransaction.status !== 200 && this.config.isPositiveFlow) {
      console.log();
      responseTransaction = await apiCallRetrySchema(call, this.config);
    }

    if (responseTransaction.status !== 200 && this.config.isPositiveFlow) {
      console.log(
        'Get Transactions by Id Response status: ',
        responseTransaction.status
      );
      throw new Error('Get Transactions by Id  API call failed');
    }

    return responseTransaction;
  }
}
