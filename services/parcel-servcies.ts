import { envs, headers, IConfig, defaultConfig } from '@utils/common.util';
import { httpRequest } from '@utils/helpers';
import { AxiosResponse } from 'axios';

export class ParcelServices {
  config: IConfig;
  parcelBody: any;
  public headers;
  parcelUrl = (merchantId?: string): string => {
    if (process.env.isPlatformUrl === 'false') {
      return `${envs?.url}/parcels/`;
    } else return `${envs?.url}/merchants/${merchantId}/parcels/`;
  };
  constructor(config?: IConfig) {
    this.parcelBody = this.createParcel();
    this.config = {
      ...defaultConfig,
      ...config
    };
    this.headers = (merchantId: string = '', conf: IConfig = this.config) =>
      headers(merchantId, conf);
  }

  async postCreateParcel(
    merchantId: string,
    body?: IParcel
  ): Promise<AxiosResponse> {
    const parcelResponse = await httpRequest(
      this.parcelUrl(merchantId),
      'POST',
      this.headers(merchantId),
      body ?? this.parcelBody
    );
    return parcelResponse;
  }

  createParcel(): IParcel {
    return {
      distance_unit: distance_unit.in,
      extra: {
        COD: {
          amount: '5.50',
          currency: 'USD',
          payment_method: 'CASH'
        },
        insurance: {
          amount: '5.50',
          content: 'Laptop',
          currency: 'USD',
          provider: 'UPS'
        }
      },
      height: '1',
      length: '1',
      mass_unit: mass_unit.lb,
      metadata: '',
      template: 'USPS_LargeFlatRateBox',
      test: true,
      weight: '1',
      width: '1'
    };
  }
}

export enum distance_unit {
  cm = 'cm',
  in = 'in',
  ft = 'ft',
  m = 'm',
  mm = 'mm',
  yd = 'yd'
}

export enum mass_unit {
  g = 'g',
  kg = 'kg',
  lb = 'lb',
  oz = 'oz'
}
export interface IParcel {
  distance_unit?: distance_unit;
  extra?: {
    COD: {
      amount: string;
      currency: string;
      payment_method: string;
    };
    insurance?: {
      amount: string;
      content: string;
      currency: string;
      provider: string;
    };
  };
  height?: string;
  length?: string;
  mass_unit?: mass_unit;
  metadata?: string;
  template?: string;
  test?: boolean;
  weight?: string;
  width?: string;
}
