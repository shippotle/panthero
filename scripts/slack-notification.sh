#!/usr/bin/env bash
channel_name=$1 # from circleci project env
message=$2

if [ $channel_name == "#qa" ]; then
    webhook_url="https://hooks.slack.com/services/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"
elif [ $channel_name == "#nightly-runs" ]; then
    webhook_url="https://hooks.slack.com/services/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"
else
    echo "no webhook url defined for $channel_name"
    exit 1
fi

echo "posting $message to $channel_name"
curl -X POST --data-urlencode "payload={\"text\": \"$message\", \"link_names\": 1}" $webhook_url