import chaiHttp from 'chai-http';
import chai from 'chai';
const deepEqualInAnyOrder = require('deep-equal-in-any-order');
chai.use(chaiHttp);
chai.use(require('chai-json-schema-ajv'));
chai.use(deepEqualInAnyOrder);

export const env = process.env.ENV ?? 'qa';

const conf =
  process.env.isPlatformUrl === 'true'
    ? {
        qa: {
          url: 'https://platform-api-dev-qa.shippodev.com',
          test_token: `${
            process.env.QA_TEST_TOKEN ??
            ''
          }`,
          live_token: `${
            process.env.QA_LIVE_TOKEN ??
            ''
          }`
        },
        prod: {
          url: 'https://platform-api.goshippo.com',
          test_token: `${process.env.PROD_TEST_TOKEN ?? ''}`,
          live_token: `${process.env.PROD_LIVE_TOKEN ?? ''}`
        },
        dev_main: {
          url: 'https://platform-api-dev-main.shippodev.com',
          test_token: `${process.env.PROD_TEST_TOKEN ?? ''}`,
          live_token: `${process.env.PROD_LIVE_TOKEN ?? ''}`
        }
      }[env]
    : {
        qa: {
          url: 'https://api-dev-qa.shippodev.com',
          test_token: `${
            process.env.QA_TEST_TOKEN ??
            ''
          }`,
          live_token: `${
            process.env.QA_LIVE_TOKEN ??
            ''
          }`
        }
      }[env];
export const envs = conf;

export const { expect } = chai;

const token = envs?.test_token ? envs?.test_token : envs?.live_token;

console.log('Test are running against URL', conf?.url);

// The following is required because some flows act differently if live or test token
process.env.TOKEN = envs?.test_token ? 'TEST' : 'PROD';

if (!token)
  throw new Error(
    'Either the Test or LIVE Platform token is required - Set it by following instruction in the readme file'
  );

export const headers = (
  merchantId: string = '',
  config: IConfig = defaultConfig
): { [key: string]: string } => {
  config = {
    ...defaultConfig,
    ...config
  };
  const finalHeaders: { [key: string]: string } = {
    Authorization: `ShippoToken ${token}`,
    'Content-Type': 'application/json'
  };
  if (process.env.isPlatformUrl === 'false' && merchantId) {
    finalHeaders['Shippo-Account-Id'] = merchantId;
  }
  if (config.live_token) {
    return {
      ...finalHeaders,
      ...{ Authorization: `ShippoToken ${config.live_token}` }
    };
  } else return finalHeaders;
};

export interface IConfig {
  stateFrom?: string;
  stateTo?: string;
  sameCity?: boolean;
  isPositiveFlow?: boolean;
  carrier?: string;
  preferred_currency?: string;
  use_preferred_currency?: boolean;
  live_token?: string;
  parcels?: [
    {
      length: string;
      width: string;
      height: string;
      distance_unit: string;
      weight: string;
      mass_unit: string;
    }
  ];
}

export const listOfCarriers = {
  ups: { carrier: 'ups', country: 'US', preferred_currency: 'USD' },
  usps: { carrier: 'usps', country: 'US', preferred_currency: 'USD' },
  hermes_uk: {
    carrier: 'hermes_uk',
    country: 'GB-UK',
    preferred_currency: 'GBP'
  },
  // dpd_uk: { carrier: 'dpd_uk', country: 'GB-UK', preferred_currency: 'GBP' },
  canada: { carrier: 'canada_post', country: 'CAN', preferred_currency: 'CAD' }
};

export const defaultConfig: IConfig = {
  stateFrom: 'CA',
  stateTo: 'CA',
  sameCity: false,
  isPositiveFlow: true,
  carrier: listOfCarriers.usps.carrier
};
