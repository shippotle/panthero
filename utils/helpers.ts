import axios, { Method, AxiosResponse } from 'axios';
import * as logger from 'npmlog';
import fs from 'fs';
import {
  requestErrorLogger,
  requestLogger,
  responseErrorLogger,
  responseLogger
} from './logger';
axios.interceptors.request.use(requestLogger, requestErrorLogger);
axios.interceptors.response.use(responseLogger, responseErrorLogger);
import { IConfig } from '@utils/common.util';

export const httpRequest = async <T, O = any>(
  url: string,
  method: Method,
  headers = {},
  data?: T
): Promise<any | unknown> => {
  let result: any | unknown;

  try {
    const response = await axios.request<O>({
      method,
      url,
      data,
      headers,
      withCredentials: true,
      timeout: 1000 * 180,
      validateStatus: () => {
        return true;
      }
    });
    result = response;
  } catch (err) {
    result = err;
    logger.error(
      'Response error:',
      `API Call to ${method} ${url} failed with ${err}`
    );
  }
  return result;
};

export const pickRandomElementFromList = (list: any) => {
  return [list[Math.floor(Math.random() * list.length)]];
};

export const pickMultipleRandomElementFromAList = (list: any, num: number) => {
  const shuffled = [...list].sort(() => 0.5 - Math.random());
  return shuffled.slice(0, num);
};

export const downloadPdf = async (url: string) => {
  console.log(url);
  const { data: pdf } = await axios.request({
    url,
    responseEncoding: 'binary',
    headers: {
      'Content-Type': 'application/pdf'
    }
  });
  await fs.writeFileSync('./test.pdf', pdf);
};

export const troubleShootPCCErrors = (
  response: AxiosResponse,
  expectedCurrency: string,
  type: 'currency' | 'currency_local' = 'currency'
): boolean => {
  const results = [];
  let error = false;
  for (const rate of response.data.rates) {
    if (type === 'currency') {
      if (rate.currency !== expectedCurrency) results.push(1);
    }
    if (type === 'currency_local') {
      if (rate.currency_local !== expectedCurrency) results.push(1);
    }
  }
  if (results.includes(1)) {
    error = true;
  }
  return error;
};

export const apiCallRetrySchema = async (
  retry: IRetrySchema,
  config?: IConfig
): Promise<AxiosResponse> => {
  let response = retry.response;
  let i = 0;

  const errors = Array.isArray(response.data.message)
    ? response.data.message.map((item: { text: any }) => item.text)
    : response.data.message;

  while (
    (![201, 200].includes(response.status) &&
      i < retry.numberOfTimes &&
      config?.isPositiveFlow) ||
    (response.data.error === retry.message && i < retry.numberOfTimes) ||
    (response.data.rates?.length === 0 && i < retry.numberOfTimes) ||
    (errors?.includes(retry.message) && i < retry.numberOfTimes)
  ) {
    await new Promise((resolve) => setTimeout(resolve, 1000));
    console.log(`Call attempt #${i + 1} failed! Trying again...`);
    response = await httpRequest(
      retry.url,
      retry.method,
      retry.headers,
      retry.body
    );
    i++;
  }

  return response;
};

interface IRetrySchema {
  response: AxiosResponse;
  url: string;
  method: Method;
  headers: any;
  body?: any;
  message?: string;
  numberOfTimes: number;
}
