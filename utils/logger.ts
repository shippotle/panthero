import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import chalk from 'chalk';
const debug = require('debug');

const debugInfo = debug('service:info');
const debugError = debug('service:error');
const debugRequest = debug('service:request');
const debugResponse = debug('service:response');

const requestColor = chalk.grey;
const responseColor = chalk.cyan;

export const requestLogger = (req: AxiosRequestConfig): AxiosRequestConfig => {
  const loggerEnvVariable: string = getEnvVariable();
  if (!loggerEnvVariable) return req;

  enableDebug(loggerEnvVariable);

  requestBeautifier(req);

  return req;
};

export const responseLogger = (res: AxiosResponse): AxiosResponse => {
  const loggerEnvVariable: string = getEnvVariable();
  if (!loggerEnvVariable) return res;

  enableDebug(loggerEnvVariable);
  responseBeautifier(res);

  return res;
};

export const requestErrorLogger = (
  error: AxiosError
): AxiosError | Promise<AxiosError> => {
  const loggerEnvVariable: string = getEnvVariable();
  if (!loggerEnvVariable) return error;

  enableDebug(loggerEnvVariable);

  if (error.request) {
    const req = requestBeautifier(error.request);
    debugError(`%s`, req);
  }
  return Promise.reject(error);
};

export const responseErrorLogger = (
  error: AxiosError
): AxiosError | Promise<AxiosError> => {
  const loggerEnvVariable: string = getEnvVariable();
  if (!loggerEnvVariable) return error;

  enableDebug(loggerEnvVariable);

  if (error.response) {
    const formattedRequest = requestFormatter(error.response.config);
    const prettifyRequest = requestColor(
      JSON.stringify(formattedRequest, null, 2)
    );
    debugError(`%s`, prettifyRequest);

    const res = responseBeautifier(error.response);
    debugError(`%s`, res);
  }

  return Promise.reject(error);
};

const requestBeautifier = (req: AxiosRequestConfig) => {
  const formattedRequest = requestFormatter(req);
  const beautifiedRequest = requestColor(
    JSON.stringify(formattedRequest, null, 2)
  );

  debugRequest(`%s`, beautifiedRequest);
  debugInfo(`%s`, beautifiedRequest);

  return beautifiedRequest;
};

const responseBeautifier = (res: AxiosResponse) => {
  const formattedResponse = responseFormatter(res);
  const beautifiedResponse = responseColor(
    JSON.stringify(formattedResponse, null, 2)
  );

  debugResponse(`%s`, beautifiedResponse);
  debugInfo(`%s`, beautifiedResponse);

  return beautifiedResponse;
};

const getEnvVariable = (): string => process.env.logger || '';

const enableDebug = (name: string): void => {
  if (!debug.enabled(`${name}`)) {
    debug.enable(`${name}`);
  }
};

const requestFormatter = (req: AxiosRequestConfig): AxiosRequestConfig => {
  const method = req.method;
  const url = req.url;
  let headers = req.headers;
  if (headers) {
    headers = Object.assign(
      // @ts-ignore
      headers.common ? { ...headers.common } : {},
      // @ts-ignore
      method ? { ...headers[method] } : {},
      {
        ...headers
      }
    );
    // remove unnecessary headers information
    ['common', 'get', 'post', 'head', 'put', 'patch', 'delete'].forEach(
      (header) => {
        if (headers?.[header]) {
          delete headers[header];
        }
      }
    );

    if (headers?.['User-Agent']) delete headers['User-Agent'];
  }

  if (method) {
    method.toUpperCase();
  }

  let body = req.data;

  if (Buffer.isBuffer(body)) {
    body = `buffer body; will not be printed`;
  }

  return Object.assign(
    {
      method,
      url
    },
    req.params ? { params: req.params } : {},
    { headers },
    body ? { body } : {},
    req.auth ? { auth: req.auth } : {}
  );
};

const responseFormatter = (res: AxiosResponse) => {
  const body = res.data;
  const headers = res.headers;
  const status = res.status;

  return {
    status,
    headers,
    body
  };
};
